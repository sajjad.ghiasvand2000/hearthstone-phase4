package hearthstone.login;

import javax.swing.*;
import java.awt.*;

public class LoginTextArea extends JPanel {
    private TextArea textArea;
    public LoginTextArea(){
        textArea = new TextArea();
        setLayout(new BorderLayout());
        add(textArea, BorderLayout.CENTER);
    }

    public void append(String s){
        textArea.append(s);
    }
}
