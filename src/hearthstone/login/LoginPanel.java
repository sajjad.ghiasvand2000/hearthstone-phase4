package hearthstone.login;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class LoginPanel extends JPanel implements ActionListener {
    private JLabel userNameLabel, passwordLabel;
    private JTextField userNameField, passwordField;
    private JButton login, register;

    public LoginPanel() {
        userNameLabel = new JLabel("Username");
        passwordLabel = new JLabel("Password");
        userNameField = new JTextField(15);
        passwordField = new JTextField(15);
        login = new JButton("Log In");
        register = new JButton("Register");
        login.addActionListener(this);
        register.addActionListener(this);
        createWindow();
    }

    private void createWindow() {

        setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        //FIRST ROW
        gc.gridy = 0;
        gc.weightx = 0.1;
        gc.weighty = 0.1;
        gc.gridx = 0;
        gc.insets = new Insets(0, 0, 0, 5);
        gc.fill = GridBagConstraints.NONE;
        gc.anchor = GridBagConstraints.LINE_END;
        add(userNameLabel, gc);
        gc.gridx = 1;
        gc.anchor = GridBagConstraints.LINE_START;
        add(userNameField, gc);

        //SECOND ROW
        gc.gridy++;
        gc.weightx = 1;
        gc.weighty = 0.1;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.LINE_END;
        add(passwordLabel, gc);
        gc.gridx = 1;
        gc.anchor = GridBagConstraints.LINE_START;
        add(passwordField, gc);

        //THIRD ROW
        gc.gridy++;
        gc.weightx = 0.1;
        gc.weighty = 0.1;
        gc.gridx = 0;
        gc.insets = new Insets(0, 50, 0, -1);
        gc.anchor = GridBagConstraints.LINE_END;
        add(login, gc);
        gc.gridx = 1;
        gc.insets = new Insets(0, 5, 0, -1);
        gc.anchor = GridBagConstraints.LINE_START;
        add(register, gc);

    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String username = userNameField.getText();
        String password = passwordField.getText();
        JButton button = (JButton) actionEvent.getSource();
        if (username.equals("") || password.equals(""))
            LoginFrame.getTextArea().append("Enter username or password.\n");
        else {
            if (button == register) {
                try {
                    new RegisterState(username, password);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (button == login) {
                try {
                    new LoginState(username, password);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
