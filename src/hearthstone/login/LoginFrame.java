package hearthstone.login;

import javax.swing.*;
import java.awt.*;

public class LoginFrame extends JFrame {

    private LoginPanel loginPanel;
    private static LoginTextArea textArea;
    private static boolean play = false;

    public LoginFrame(){
        super("Login");
        loginPanel = new LoginPanel();
        textArea = new LoginTextArea();
        add(textArea, BorderLayout.SOUTH);
        add(loginPanel, BorderLayout.CENTER);
        setResizable(false);
        setSize(500, 500);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        getContentPane().setBackground(Color.DARK_GRAY);
        setLayout(new BorderLayout());

    }

    public static LoginTextArea getTextArea() {
        return textArea;
    }

    public static boolean isPlay() {
        return play;
    }

    public static void setPlay(boolean play) {
        LoginFrame.play = play;
    }
}
