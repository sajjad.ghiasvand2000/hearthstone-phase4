package hearthstone.login;

import server.Entity.Deck;
import server.Entity.cards.Card;
import server.Entity.cards.minion.Minion;
import server.Entity.cards.spell.Spell;
import server.Entity.cards.weapon.Weapon;
import com.google.gson.Gson;
import server.Entity.MainPlayer;
import com.google.gson.internal.LinkedTreeMap;
import server.data.Log;
import server.data.Setter;
import hearthstone.Game;
import hearthstone.Launcher;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

public class LoginState {
    private String username;
    private String password;

    public LoginState(String username, String password) throws IOException {
        this.username = username;
        this.password = password;
        init();
    }

    private void init() throws IOException {
        Scanner fileReader = new Scanner(new FileReader("player.txt"));
        while (fileReader.hasNext()) {
            Gson gson = new Gson();
            Map map = gson.fromJson(fileReader.nextLine(), Map.class);
            if (username.equals(map.get("username")) && password.equals(map.get("password"))) {
                ArrayList<Card> entireCards = new ArrayList<>();
                ArrayList<Deck> allDecks = new ArrayList<>();
                Double userId = (Double) map.get("userId");
                Double diamond = (Double) map.get("diamond");
                ArrayList cards = (ArrayList) map.get("entireCards");
                ArrayList decks = (ArrayList) map.get("decks");
                for (int i = 0; i < 17; i++) {
                    LinkedTreeMap linkedTreeMap = (LinkedTreeMap) cards.get(i);
                    Minion minion = (Minion) Minion.factory(linkedTreeMap);
                    entireCards.add(minion);
                }
                for (int i = 17; i < 32; i++) {
                    LinkedTreeMap linkedTreeMap = (LinkedTreeMap) cards.get(i);
                    entireCards.add((Spell) Spell.factory(linkedTreeMap));
                }
                for (int i = 32; i < 37; i++) {
                    LinkedTreeMap linkedTreeMap = (LinkedTreeMap) cards.get(i);
                    entireCards.add((Weapon) Weapon.factory(linkedTreeMap));
                }
                MainPlayer.getInstance().setUserId(userId.intValue());
                MainPlayer.getInstance().setUsername((String) map.get("username"));
                MainPlayer.getInstance().setPassword((String) map.get("password"));
                MainPlayer.getInstance().setHeroes(Setter.setHero());
                for (int i = 0; i < 13; i++) {
                    LinkedTreeMap linkedTreeMap = (LinkedTreeMap) decks.get(i);
                    if (linkedTreeMap != null) {
                        allDecks.add((Deck) Deck.factory(linkedTreeMap));
                        System.out.println(1);
                    }
                    else allDecks.add(null);
                }
                MainPlayer.getInstance().setEntireCards(entireCards);
                MainPlayer.getInstance().setDecks(allDecks.toArray(new Deck[13]));
                MainPlayer.getInstance().setDiamond(diamond.intValue());
                Log.body("login", "successful login");
                fileReader.close();
                LoginFrame.getTextArea().append("Congratulation!Your username and password were true.\n");
                Launcher.setGame(new Game(1700, 1000, "Hearthstone"));
                Launcher.getGame().start();
                return;
            }
        }
        fileReader.close();
        LoginFrame.getTextArea().append("You Have Entered Wrong Username Or Password.\nPlease Try Again.\n");
    }
}
