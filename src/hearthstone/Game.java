package hearthstone;

import hearthstone.display.Display;
import hearthstone.gfx.Asserts;
import hearthstone.input.KeyManager;
import hearthstone.input.MouseManager;
import hearthstone.states.*;
import hearthstone.states.changeCardState.ChangeCardState;
import hearthstone.states.collectionState.CollectionState;
import hearthstone.states.infoPassive.InfoPassive;
import hearthstone.states.menuState.MenuState;
import hearthstone.states.playState.graphic.PlayState;
import hearthstone.states.shopState.ShopState;
import hearthstone.states.statusState.graphic.StatusState;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.io.IOException;

public class Game implements Runnable {

    private Display display;
    private int width, height;
    private String title;
    private Thread thread;
    private boolean running = false;
    private BufferStrategy bs;
    private Graphics g;
    private Handler handler;

    //INPUT
    private static MouseManager mouseManager;
    private KeyManager keyManager;

    //STATES
    private MenuState menuState;
    private CollectionState collectionState;
    private ShopState shopState;
    private StatusState statusState;
    private PlayState playState;
    private InfoPassive infoPassive1;
    private InfoPassive infoPassive2;
    private ChangeCardState changeCardState1;
    private ChangeCardState changeCardState2;

    public Game(int width, int height, String title) {
        this.width = width;
        this.height = height;
        this.title = title;
        keyManager = new KeyManager();
        mouseManager = new MouseManager();
    }

    private void init() throws IOException {
        Asserts.init();
        display = new Display(title, width, height);
        display.getFrame().addKeyListener(keyManager);
        display.getFrame().addMouseListener(mouseManager);
        display.getFrame().addMouseMotionListener(mouseManager);
        display.getFrame().requestFocus();
        display.getFrame().setFocusable(true);
        display.getFrame().setFocusTraversalKeysEnabled(false);
        display.getCanvas().addMouseListener(mouseManager);
        display.getCanvas().addKeyListener(keyManager);
        display.getCanvas().addMouseMotionListener(mouseManager);
        display.getCanvas().setFocusable(true);
        display.getCanvas().setFocusTraversalKeysEnabled(false);
        display.getCanvas().requestFocus();
        handler = new Handler(this);
        menuState = new MenuState(handler);
        collectionState = new CollectionState(handler);
        shopState = new ShopState(handler);
        State.setCurrentState(menuState);
    }


    private void tick(){
        keyManager.tick();
        mouseManager.tick();
        if (State.getCurrentState() != null)
            State.getCurrentState().tick();

    }

    private void render(){
        bs = display.getCanvas().getBufferStrategy();
        if (bs == null){
            display.getCanvas().createBufferStrategy(3);
            return;
        }
        g = bs.getDrawGraphics();
        Graphics2D g2D = (Graphics2D)g;
        g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //CLEAR SCREEN
        g2D.clearRect(0, 0, width, height);
        //DRAW HERE
        if (State.getCurrentState() != null)
            State.getCurrentState().render(g2D);

        //END DRAWING
        bs.show();
        g2D.dispose();


    }


    @Override
    public void run() {
        try {
            init();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int fps = 60;
        double timePerTick = (double)1000000000/fps;
        double delta = 0;
        long now;
        long lastTime = System.nanoTime();
        while (running){
            now = System.nanoTime();
            delta += (now - lastTime)/timePerTick;
            lastTime = now;
            if (delta >= 1) {
                tick();
                render();
                delta = 0;
            }
        }

        try {
            stop();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public synchronized void start(){
        if (running)
            return;
        running = true;
        thread = new Thread(this);
        thread.start();
    }

    public synchronized void stop() throws InterruptedException {
        if (!running)
            return;
        running = false;
        thread.join();

    }

    public Display getDisplay() {
        return display;
    }

    public CollectionState getCollectionState() {
        return collectionState;
    }

    public ShopState getShopState() {
        return shopState;
    }

    public static MouseManager getMouseManager() {
        return mouseManager;
    }

    public  KeyManager getKeyManager() {
        return keyManager;
    }

    public MenuState getMenuState() {
        return menuState;
    }

    public void setPlayState(PlayState playState) {
        this.playState = playState;
    }

    public PlayState getPlayState() {
        return playState;
    }

    public InfoPassive getInfoPassive1() {
        return infoPassive1;
    }

    public void setInfoPassive1(InfoPassive infoPassive1) {
        this.infoPassive1 = infoPassive1;
    }

    public InfoPassive getInfoPassive2() {
        return infoPassive2;
    }

    public void setInfoPassive2(InfoPassive infoPassive2) {
        this.infoPassive2 = infoPassive2;
    }

    public void setStatusState(StatusState statusState) {
        this.statusState = statusState;
    }

    public StatusState getStatusState() {
        return statusState;
    }

    public ChangeCardState getChangeCardState1() {
        return changeCardState1;
    }

    public void setChangeCardState1(ChangeCardState changeCardState1) {
        this.changeCardState1 = changeCardState1;
    }

    public ChangeCardState getChangeCardState2() {
        return changeCardState2;
    }

    public void setChangeCardState2(ChangeCardState changeCardState2) {
        this.changeCardState2 = changeCardState2;
    }
}
