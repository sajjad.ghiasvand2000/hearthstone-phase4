package hearthstone.states.statusState.logic;

import server.Entity.Deck;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class DeckComparator implements Comparator<Deck> {

    @Override
    public int compare(Deck deck, Deck t1) {
        int compare2 = compareWinDividedByAllGameDeck(deck, t1);
        int compare3 = compareWinDeck(deck, t1);
        int compare4 = compareGameDeck(deck, t1);
        int compare5 = compareMeanManaDeck(deck, t1);
        if (compare2 != 0)
            return compare2;
        else if (compare3 != 0)
            return compare3;
        else if (compare4 != 0)
            return compare4;
        else if (compare5 != 0)
            return compare5;
        else return produceRandomOneOrMinesOne();
    }

    private int compareWinDividedByAllGameDeck(Deck deck, Deck t1){
        float n1 = deck.getNumberGame();
        float n2 = t1.getNumberGame();
        float w1 = deck.getNumberWin();
        float w2 = t1.getNumberWin();
        float a1, a2;
        if (n1 == 0) a1 = 0;
        else a1 = w1 / n1;
        if (n2 == 0) a2 = 0;
        else a2 = w2 / n2;
        return Float.compare(a1, a2);
    }

    private int compareWinDeck(Deck deck, Deck t1){
        int w1 = deck.getNumberWin();
        int w2 = t1.getNumberWin();
        return Integer.compare(w1, w2);
    }

    private int compareGameDeck(Deck deck, Deck t1){
        int g1 = deck.getNumberGame();
        int g2 = t1.getNumberGame();
        return Integer.compare(g1, g2);
    }

    private int compareMeanManaDeck(Deck deck, Deck t1){
        float m1 = StatusLogic.meanOfMamaDeck(deck);
        float m2 = StatusLogic.meanOfMamaDeck(t1);
        return Float.compare(m1, m2);
    }

    private int produceRandomOneOrMinesOne(){
        ArrayList<Integer> list = new ArrayList<>();
        Collections.addAll(list, 1, -1);
        Collections.shuffle(list);
        return list.get(0);
    }
}
