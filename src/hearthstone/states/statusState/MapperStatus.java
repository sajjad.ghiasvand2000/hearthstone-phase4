package hearthstone.states.statusState;

import server.Entity.Deck;
import hearthstone.states.statusState.logic.StatusLogic;
import java.util.List;

public class MapperStatus {
    private StatusLogic statusLogic;

    public MapperStatus(){
        statusLogic = new StatusLogic();
    }

    public List<Deck> getComparedDecks(){
        return statusLogic.getContainComparedDecks();
    }
}
