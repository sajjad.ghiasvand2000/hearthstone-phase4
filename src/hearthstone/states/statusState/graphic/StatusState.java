package hearthstone.states.statusState.graphic;

import server.data.Log;
import hearthstone.Handler;
import hearthstone.configs.MenuStateConfigs;
import hearthstone.configs.PassiveStateConfigs;
import hearthstone.constants.Constants;
import hearthstone.gfx.Asserts;
import hearthstone.states.State;
import hearthstone.states.statusState.MapperStatus;
import hearthstone.ui.UIManager;
import hearthstone.ui.UIRecImage;
import hearthstone.utils.Util;
import java.awt.*;

public class StatusState extends State {
    private static MenuStateConfigs m = MenuStateConfigs.getInstance();
    private static PassiveStateConfigs p = PassiveStateConfigs.getInstance();
    private MapperStatus mapper;
    private UIManager uiManager;

    public StatusState(Handler handler) {
        super(handler);
        mapper = new MapperStatus();
        uiManager = new UIManager(handler);
        menu();
        Util.exit(uiManager);
    }

    @Override
    public void tick() {
        uiManager.tick();
    }

    @Override
    public void render(Graphics2D g2D) {
        g2D.drawImage(Asserts.statusBackground, 0, 0, Constants.COMPUTER_WIDTH, Constants.COMPUTER_HEIGHT, null);
        int height = p.get("initialHeightStatus");
        for (int i = 0 ; i < Math.min(mapper.getComparedDecks().size(), 10) ; i++) {
            drawer(g2D, mapper.getComparedDecks().get(i).toString(), height);
            height += p.get("horizontalDistance");
        }
        uiManager.render(g2D);
    }

    private void drawer(Graphics2D g2D, String prompt, int height) {
        Font font = new Font("Helvetica", Font.BOLD, 20);
        FontMetrics fontMetrics = g2D.getFontMetrics(font);
        int width = fontMetrics.stringWidth(prompt);
        g2D.setColor(Color.CYAN);
        g2D.setFont(font);
        g2D.drawString(prompt, (Constants.COMPUTER_WIDTH - width) / 2, height);
    }

    private void menu() {
        uiManager.addButton(new UIRecImage(m.getMenuSateConfigs("initialXPosMenu"), m.getMenuSateConfigs("initialYPosMenu"),
                m.getMenuSateConfigs("widthMenu"), m.getMenuSateConfigs("heightMenu"), Asserts.menu, () -> {
            State.setCurrentState(handler.getMenuState());
            handler.getCollectionState().updateCardInit();
            Log.body("click menu button", "back to menu sate");
        }));
    }
}
