package hearthstone.states.menuState;

import server.Entity.MainPlayer;
import server.data.Log;
import hearthstone.Handler;
import hearthstone.configs.MenuStateConfigs;
import hearthstone.constants.Constants;
import hearthstone.gfx.Asserts;
import hearthstone.states.State;
import hearthstone.states.infoPassive.InfoPassive;
import hearthstone.states.playState.graphic.PlayState;
import hearthstone.ui.UIRecImage;
import hearthstone.ui.UIManager;
import hearthstone.states.statusState.graphic.StatusState;
import hearthstone.utils.Util;
import java.awt.*;

public class MenuState extends State {
    private MenuStateConfigs menuStateConfigs;
    private UIManager uiManager;
    private PlayState playState;
    private InfoPassive infoPassive;
    private StatusState statusState;

    public MenuState(Handler handler) {
        super(handler);
        uiManager = new UIManager(handler);
        menuStateConfigs = MenuStateConfigs.getInstance();
        playInit();
        shopInit();
        statusInit();
        collectionInit();
        Util.exit(uiManager);
    }

    @Override
    public void tick() {
        uiManager.tick();
    }

    @Override
    public void render(Graphics2D g2D) {
        g2D.drawImage(Asserts.menuBackground, 0, 0, Constants.COMPUTER_WIDTH, Constants.COMPUTER_HEIGHT, null);
        uiManager.render(g2D);
    }

    private void playInit() {
        uiManager.addButton(new UIRecImage(menuStateConfigs.getMenuSateConfigs("initialXPos"), menuStateConfigs.getMenuSateConfigs("initialYPos")
                , menuStateConfigs.getMenuSateConfigs("width"), menuStateConfigs.getMenuSateConfigs("height")
                , Asserts.play, () -> {
            Log.body("click play", "click for playing in menu state");
            if (MainPlayer.getInstance().getDecks()[0] == null)
                State.setCurrentState(handler.getCollectionState());
            else {
                infoPassive = new InfoPassive(handler);
                handler.setInfoPassive1(infoPassive);
                State.setCurrentState(infoPassive);
            }
        }));
    }

    private void collectionInit() {
        uiManager.addButton(new UIRecImage(menuStateConfigs.getMenuSateConfigs("initialXPos"), menuStateConfigs.getMenuSateConfigs("initialYPos") +
                menuStateConfigs.getMenuSateConfigs("height") + menuStateConfigs.getMenuSateConfigs("verticalDistance"),
                menuStateConfigs.getMenuSateConfigs("width"), menuStateConfigs.getMenuSateConfigs("height"), Asserts.myCollectionIcon, () -> {
            State.setCurrentState(handler.getCollectionState());
            Log.body("click myCollection button", "click for going to collection state");
        }));
    }

    private void shopInit() {
        uiManager.addButton(new UIRecImage(menuStateConfigs.getMenuSateConfigs("initialXPos"), menuStateConfigs.getMenuSateConfigs("initialYPos") +
                2 * menuStateConfigs.getMenuSateConfigs("height") + 2 * menuStateConfigs.getMenuSateConfigs("verticalDistance"),
                menuStateConfigs.getMenuSateConfigs("width"), menuStateConfigs.getMenuSateConfigs("height"), Asserts.shop, () -> {
            State.setCurrentState(handler.getShopState());
            Log.body("click shop button", "click for going to shop state");
        }));
    }

    private void statusInit() {
        uiManager.addButton(new UIRecImage(menuStateConfigs.getMenuSateConfigs("initialXPos"), menuStateConfigs.getMenuSateConfigs("initialYPos") +
                3 * menuStateConfigs.getMenuSateConfigs("height") + 3 * menuStateConfigs.getMenuSateConfigs("verticalDistance"),
                menuStateConfigs.getMenuSateConfigs("width"), menuStateConfigs.getMenuSateConfigs("height"), Asserts.status, () -> {
            Log.body("click status button", "click for going to status state");
            statusState = new StatusState(handler);
            handler.setStatusState(statusState);
            State.setCurrentState(statusState);
        }));
    }
}
