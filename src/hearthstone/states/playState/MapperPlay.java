package hearthstone.states.playState;

import server.Entity.cards.Card;
import server.Entity.Deck;
import server.Entity.cards.minion.Minion;
import server.Entity.cards.spell.QuestReward;
import server.Entity.cards.spell.Spell;
import server.Entity.cards.weapon.Weapon;
import server.Entity.hero.Hero;
import server.Entity.hero.HeroClass;
import hearthstone.Handler;
import hearthstone.states.playState.logic.GamePlayerLogic;

import java.util.ArrayList;

public class MapperPlay {
    private Handler handler;
    private GamePlayerLogic gamePlayerLogic;
    private String name;

    public MapperPlay(Handler handler, String name) {
        this.handler = handler;
        this.name = name;
        gamePlayerLogic = new GamePlayerLogic(handler, name);
    }

    public void clickEndTurn() {
        gamePlayerLogic.clickEndTurn();
    }

    public void addCardToHand() {
        gamePlayerLogic.addCardToHand();
    }

    public void clickDeckCard(Card card) {
        gamePlayerLogic.clickDeckCard(card);
    }

    public int getMana() {
        return gamePlayerLogic.getMana();
    }

    public ArrayList<Card> getContainDeckCard() {
        return gamePlayerLogic.getContainDeckCard();
    }

    public int getLevel() {
        return gamePlayerLogic.getLevel();
    }

    public Deck getDeck() {
        return gamePlayerLogic.getDeck();
    }

    public ArrayList<String> getEvent() {
        return gamePlayerLogic.getEvent();
    }

    public Minion getMinionWantedSummon() {
        return gamePlayerLogic.getMinionLogic().getMinionWantSummon();
    }

    public Minion getMinionWantAttack() {
        return gamePlayerLogic.getMinionLogic().getMinionWantAttack();
    }

    public void removeDeckMinion(Minion minion, int i) {
        gamePlayerLogic.getMinionLogic().removeDeckMinion(minion, i);
    }

    public void clickLandCard(int i) {
        gamePlayerLogic.getMinionLogic().clickLandCard(i);
    }

    public void endClickLandCard() {
        gamePlayerLogic.getMinionLogic().endClickLandCard();
    }

    public Minion[] getContainLandCard() {
        return gamePlayerLogic.getContainLandCard();
    }

    public GamePlayerLogic getGamePlayerLogic() {
        return gamePlayerLogic;
    }

    public void makeMinionsAttackable() {
        gamePlayerLogic.getMinionLogic().makeMinionsAttackable();
    }

    public void changeTurn(String name) {
        gamePlayerLogic.changeTurn(name);
    }

    public boolean isTurn() {
        return gamePlayerLogic.isTurn();
    }

    public int getUpdateFlagACardInLand() {
        return gamePlayerLogic.getUpdateFlagACardInLand();
    }

    public void changeUpdateFlagACardInLand() {
        gamePlayerLogic.changeUpdateFlagACardInLand();
    }

    public void checkHPLandCards(int i) {
        gamePlayerLogic.checkHPLandCards(i);
    }

    public void heroPower() {
        gamePlayerLogic.getHeroLogic().heroPower();
    }

    public boolean isSpecialUpdateFlag() {
        return gamePlayerLogic.isSpecialUpdateFlag();
    }

    public void changeSpecialUpdateFlag() {
        gamePlayerLogic.changeSpecialUpdateFlag();
    }

    public int getIndexMinionWantAttack() {
        return gamePlayerLogic.getMinionLogic().getIndexMinionWantAttack();
    }

    public Spell getSpellWantAttack() {
        return gamePlayerLogic.getSpellLogic().getSpellWantAttack();
    }

    public Hero getHero() {
        return gamePlayerLogic.getDeck().getHero();
    }

    public HeroClass getHeroClass() {
        return gamePlayerLogic.getDeck().getHero().getHeroClass();
    }

    public Card getDeckCard(int i) {
        return gamePlayerLogic.getDeck().getCards().get(i);
    }

    public int getDeckCardSize() {
        return gamePlayerLogic.getDeck().getCards().size();
    }

    public boolean isUpdateFlagDeck() {
        return gamePlayerLogic.isUpdateFlagDeck();
    }

    public void changeUpdateFlagDeck() {
        gamePlayerLogic.changeUpdateFlagDeck();
    }

    public Card getExtraCard() {
        return gamePlayerLogic.getExtraCard();
    }

    public void changeExtraCard() {
        gamePlayerLogic.changeExtraCard();
    }

    public int getNumberOfCards() {
        return gamePlayerLogic.getNumberOfCards();
    }

    public void clickOnHero() {
        gamePlayerLogic.getHeroLogic().clickOnHero();
    }

    public void dealSpell(Spell spell) {
        gamePlayerLogic.getSpellLogic().dealSpell(spell);
    }

    public boolean isHeroAttack() {
        return gamePlayerLogic.getHeroLogic().isHeroAttack();
    }

    public void changeHeroAttack() {
        gamePlayerLogic.getHeroLogic().changeHeroAttack();
    }

    public boolean isHeroPowerWantAttack() {
        return gamePlayerLogic.getHeroLogic().isHeroPowerWantAttack();
    }

    public boolean heroPowerAttackMinion(Minion minion) {
        return gamePlayerLogic.getHeroLogic().heroPowerAttackMinion(minion);
    }

    public void changeHeroPowerWantAttack() {
        gamePlayerLogic.getHeroLogic().changeHeroPowerWantAttack();
    }

    public boolean heroPowerAttackHero(Hero hero) {
        return gamePlayerLogic.getHeroLogic().heroPowerAttackHero(hero);
    }

    public boolean isHeroUpdateFlag() {
        return gamePlayerLogic.isHeroUpdateFlag();
    }

    public void changeHeroUpdateFlag() {
        gamePlayerLogic.changeHeroUpdateFlag();
    }

    public String getErrorMassage() {
        return gamePlayerLogic.getErrorMassage();
    }

    public boolean isError(){
        return gamePlayerLogic.isError();
    }

    public void changeError(){
        gamePlayerLogic.changeError();
    }

    public void changeErrorMassage(){
        gamePlayerLogic.changeErrorMassage();
    }

    public Weapon getWeapon(){
        return gamePlayerLogic.getWeaponLogic().getWeapon();
    }

    public void changeUpdateWeaponFlag(){
        gamePlayerLogic.changeUpdateWeaponFlag();
    }

    public boolean isUpdateWeaponFlag(){
        return gamePlayerLogic.isUpdateWeaponFlag();
    }

    public void weaponAttack(){
        gamePlayerLogic.getWeaponLogic().weaponAttack();
    }

    public void changeWeaponWantAttack(){
        gamePlayerLogic.getWeaponLogic().changeWeaponWantAttack();
    }

    public boolean isWeaponWantAttack(){
        return gamePlayerLogic.getWeaponLogic().isWeaponWantAttack();
    }

    public boolean quest(){
        return gamePlayerLogic.getQuestReward().quest(gamePlayerLogic);
    }

    public void reward(){
        gamePlayerLogic.getQuestReward().reward(gamePlayerLogic);
    }

    public String developmentPercent(){
        return Double.toString(gamePlayerLogic.getQuestReward().developmentPercent(gamePlayerLogic));
    }

    public QuestReward getQuestReward() {
        return gamePlayerLogic.getQuestReward();
    }

    public void changeQuestReward(){
        gamePlayerLogic.changeQuestReward();
    }

    public void checkDurability(){
        gamePlayerLogic.checkDurability();
    }

    public String getName(){
        return gamePlayerLogic.getName();
    }
}
