package hearthstone.states.playState.logic;

import server.Entity.cards.Card;
import server.Entity.cards.Type;
import server.Entity.cards.weapon.Weapon;
import server.data.Log;

import java.util.ArrayList;

public class WeaponLogic {

    private GamePlayerLogic GPL;
    private Weapon weapon;
    private ArrayList<Card> containDeckCard;
    private boolean canUseWeapon = true;
    private boolean weaponWantAttack;

    public WeaponLogic(GamePlayerLogic GPL) {
        this.GPL = GPL;
        containDeckCard = GPL.getContainDeckCard();
    }

    public void clickOnWeapon(Card card){
        if (card.getType().equals(Type.WEAPON)){
            Log.body(GPL.getName() + ":a weapon clicked.", card.getName());
            weapon = (Weapon)card;
            boolean checkMana = GPL.checkMana(card);
            if (checkMana && GPL.isTurn()) {
                containDeckCard.remove(card);
                GPL.decreaseMana(card);
                GPL.changeUpdateWeaponFlag();
            }else if (!checkMana){
                Log.body(GPL.getName() + ":mana was not enough", card.getName());
                GPL.setErrorMassage("You don't have enough mana!");
            }else if (!GPL.isTurn()){
                GPL.setErrorMassage("It is not the turn of " + GPL.getName());
            }
        }
    }

    public void weaponAttack() {
        if (canUseWeapon && GPL.isTurn()) {
            weaponWantAttack = true;
            canUseWeapon = false;
        }else if (!canUseWeapon){
            GPL.setErrorMassage("You can not use this weapon any more!");
        }else if (!GPL.isTurn()){
            GPL.setErrorMassage("It is not the turn of " + GPL.getName());
        }
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setCanUseWeapon(boolean canUseWeapon) {
        this.canUseWeapon = canUseWeapon;
    }

    public void changeWeaponWantAttack(){
        weaponWantAttack = !weaponWantAttack;
    }

    public boolean isWeaponWantAttack() {
        return weaponWantAttack;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }
}
