package hearthstone.states.playState.logic;

import server.Entity.Deck;
import server.Entity.cards.minion.Minion;
import server.Entity.hero.Hero;
import server.data.Log;

public class HeroLogic {
    private boolean heroAttack = false;
    private boolean canUseHeroPower = true;
    private GamePlayerLogic GPL;
    private Deck deck;
    private boolean heroPowerWantAttack;
    private int howManyHeroPower = 0;

    public HeroLogic(GamePlayerLogic gamePlayerLogic){
        this.GPL = gamePlayerLogic;
        deck = GPL.getDeck();
    }
    public void clickOnHero() {
        if (!GPL.isTurn()) {
            heroAttack = true;
        }
    }

    public void changeHeroAttack() {
        heroAttack = !heroAttack;
    }

    public void heroPower() {
        if (canUseHeroPower && GPL.isTurn()) {
            Log.body(GPL.getName() + ": first time for attacking in this turn", deck.getHero().getHeroClass().toString());
            deck.getHero().heroPower(GPL);
            howManyHeroPower++;
            canUseHeroPower = GPL.getFreePowerPassive() == 1 && howManyHeroPower == 1;
        }else if (!canUseHeroPower){
            GPL.setErrorMassage("You can not use your hero power any more!");
        }else if (!GPL.isTurn()){
            GPL.setErrorMassage("It is not the turn of " + GPL.getName());
        }
    }

    public boolean heroPowerAttackMinion(Minion minion){
        if (GPL.getMana() > 1) {
            minion.setHP(minion.getHP() - 1);
            GPL.getMinionLogic().checkPyromaniac(minion);
            GPL.setMana(GPL.getMana() - 2 + GPL.getFreePowerPassive());
            Log.body(GPL.getName() + ":hero power want attack a minion", minion.getName());
            return true;
        }
        return false;
    }

    public boolean heroPowerAttackHero(Hero hero){
        if (GPL.getMana() > 1){
            hero.setHP(hero.getHP() - 1);
            GPL.setMana(GPL.getMana() - 2 + GPL.getFreePowerPassive());
            Log.body(GPL.getName() + ":hero power want attack hero", hero.getHeroClass().toString());
            return true;
        }
        return false;
    }

    public void changeHeroPowerWantAttack(){
        heroPowerWantAttack = !heroPowerWantAttack;
    }

    public boolean isHeroAttack() {
        return heroAttack;
    }

    public void setHeroAttack(boolean heroAttack) {
        this.heroAttack = heroAttack;
    }

    public void setCanUseHeroPower(boolean canUseHeroPower) {
        this.canUseHeroPower = canUseHeroPower;
    }

    public boolean isHeroPowerWantAttack() {
        return heroPowerWantAttack;
    }

    public void setHeroPowerWantAttack(boolean heroPowerWantAttack) {
        this.heroPowerWantAttack = heroPowerWantAttack;
    }

    public void setHowManyHeroPower(int howManyHeroPower) {
        this.howManyHeroPower = howManyHeroPower;
    }
}
