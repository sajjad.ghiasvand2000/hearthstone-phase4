package hearthstone.states.playState.logic;

import server.Entity.cards.Card;
import server.Entity.cards.minion.*;
import server.data.Log;
import hearthstone.utils.Util;

import java.util.ArrayList;

public class MinionLogic {

    private Minion MinionWantSummon = null;
    private Minion MinionWantAttack = null;
    private int indexMinionWantAttack;
    private Minion[] register1;
    private Minion[] register2;
    private Minion minionRegister;
    private GamePlayerLogic GPL;
    private ArrayList<Card> containDeckCard;
    private Minion[] containLandCard;
    private boolean opponentHunter = false;
    private int minionQuest;
    private Minion Sathrovarr = null;
    private boolean SathrovarrDetection;

    public MinionLogic(GamePlayerLogic gamePlayerLogic) {
        this.GPL = gamePlayerLogic;
        register1 = new Minion[7];
        register2 = new Minion[7];
        containLandCard = GPL.getContainLandCard();
        containDeckCard = GPL.getContainDeckCard();
    }

    public void clickOnMinion(Card card) {
        Minion minion;
        if (card.getType().toString().equals("MINION")) {
            Log.body(GPL.getName() + ":a minion clicked.", card.getName());
            minion = (Minion) card;
            if (GPL.checkMana(minion)) {
                Log.body(GPL.getName() + ":mana was enough.", card.getName());
                MinionWantSummon = minion;
                minionRegister = minion;
            } else {
                Log.body(GPL.getName() + ":mana was not enough", card.getName());
                GPL.setErrorMassage("You don't have enough mana!");
            }
        }
    }

    public void removeDeckMinion(Minion minion, int i) {
        GPL.decreaseMana(minion);
        decreaseHP(minion, i);
        Card card1 = GPL.finedCardByRef(minion);
        containDeckCard.remove(card1);//todo
        if (minion.getName().equals("Sathrovarr")) {
            SathrovarrDetection = true;
        }
        setFirstHandWithCheckingCharge(minion);
        setTaunt(minion);
        GPL.setContainLandCard(minion, i);
        if (minion.getDescription().contains("Battlecry")) {
            ((Battlecry) minion).battlecry(GPL);
            Log.body(GPL.getName() + ":minion had battlecry.", minion.getName());
        }
        MinionWantSummon = null;
        GPL.getEvent().add(minion.getName());
    }

    public void decreaseHP(Minion minion, int i) {
        if (opponentHunter) {
            minion.setHP(minion.getHP() - 1);
            checkPyromaniac(minion);
            GPL.setUpdateFlagACardInLand(i);
            Log.body(GPL.getName() + ":your opponent is hunter", "decrease 1 HP.");
        }
    }

    public void checkCurioCollector(Minion minion) {
        Minion minion1 = checkMinionInLand("Curio Collector");
        if (minion1 != null) {
            ((CurioCollector) minion1).gain(minion);
            Log.body(GPL.getName() + ":there is Curio Collector.", minion.getName());
        }
    }

    public void checkShotbot(int i) {
        if (containLandCard[i] != null) {
            if (containLandCard[i].getName().equals("Shotbot")) {
                ((Shotbot) containLandCard[i]).reborn();
                Log.body(GPL.getName() + ":shotbot was reborn", "");
            }
        }
    }

    public void checkPyromaniac(Minion minion1) {
        Minion minion = checkMinionInLand("Pyromaniac");
        if (minion != null) {
            ((Pyromaniac) minion).drawCardIfHeroPowerKillAMinion(minion1, GPL);
            Log.body(GPL.getName() + ":there is Pyromaniac.", minion1.getName());
        }
    }

    public void checkGadgetzanAuctioneer() {
        Minion minion = checkMinionInLand("Gadgetzan Auctioneer");
        if (minion != null) {
            ((GadgetzanAuctioneer) minion).castSpellThenDrawACard(GPL);
            Log.body(GPL.getName() + "a card was drawn because of Gadgetzan Auctioneer.", "");
        }
    }

    public void checkMoargArtificer() {
        Minion minion = checkMinionInLand("Moarg Artificer");
        if (minion != null) {
            ((MoargArtificer) minion).doubleDamage(GPL);
        }
    }

    private void checkSathrovarr(int i) {
        Sathrovarr = containLandCard[i];
        ((Sathrovarr) Util.findCardFromMainPlayer("Sathrovarr")).battlecry(GPL);
        SathrovarrDetection = false;
    }

    private Minion checkMinionInLand(String name) {
        for (Minion minion : containLandCard) {
            if (minion != null) {
                if (minion.getName().equals(name)) {
                    return minion;
                }
            }
        }
        return null;
    }

    public void clickLandCard(int i) {
        PlayStateLogic.setWhichMinionWantAttack(GPL.getName());
        if (SathrovarrDetection){
            checkSathrovarr(i);
        }else if (register1[i] == null) {
            MinionWantAttack = containLandCard[i];
            indexMinionWantAttack = i;
            register2[i] = containLandCard[i];
            Log.body(GPL.getName() + " :setting properties.", "MinionWantAttack = " + containLandCard[i].getName());
        }
    }

    private void setFirstHandWithCheckingCharge(Minion minion) {
        if (!minion.getName().equals("Bazaar Mugger") && !minion.getName().equals("Kayn Sunfury") &&
                !minion.getName().equals("Swamp King Dred") && !minion.getName().equals("Gadgetzan Auctioneer")) {
            Log.body(GPL.getName() + "this minion is not charge.", minion.getName());
            minion.setCharge(true);
            minion.setRush(true);
        }
    }

    private void setTaunt(Minion minion) {
        if (minion.getName().equals("Tomb Warden") || minion.getName().equals("Pit Commander"))
            minion.setTaunt(true);
    }

    public void makeMinionsAttackable() {
        for (Minion minion : containLandCard) {
            if (minion != null) {
                minion.setCharge(false);
                minion.setRush(false);
            }
        }
    }

    public void endClickLandCard() {
        MinionWantAttack = null;
    }

    public Minion getMinionWantSummon() {
        return MinionWantSummon;
    }

    public void setMinionWantSummon(Minion minionWantSummon) {
        MinionWantSummon = minionWantSummon;
    }

    public Minion getMinionWantAttack() {
        return MinionWantAttack;
    }

    public void setMinionWantAttack(Minion minionWantAttack) {
        MinionWantAttack = minionWantAttack;
    }

    public int getIndexMinionWantAttack() {
        return indexMinionWantAttack;
    }

    public Minion[] getRegister1() {
        return register1;
    }

    public void setRegister1(Minion[] register1) {
        this.register1 = register1;
    }

    public Minion[] getRegister2() {
        return register2;
    }

    public void setRegister2(Minion[] register2) {
        this.register2 = register2;
    }

    public Minion getMinionRegister() {
        return minionRegister;
    }

    public void setOpponentHunter(boolean opponentHunter) {
        this.opponentHunter = opponentHunter;
    }

    public int getMinionQuest() {
        return minionQuest;
    }

    public void setMinionQuest(int minionQuest) {
        this.minionQuest = minionQuest;
    }

    public Minion getSathrovarr() {
        return Sathrovarr;
    }

    public void setSathrovarr(Minion sathrovarr) {
        Sathrovarr = sathrovarr;
    }
}
