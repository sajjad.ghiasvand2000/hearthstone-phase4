package hearthstone.states.playState.logic;

import server.Entity.Deck;
import server.Entity.cards.Card;
import server.Entity.cards.Type;
import server.Entity.cards.spell.ComplexSpell;
import server.Entity.cards.spell.QuestReward;
import server.Entity.cards.spell.SingleSpell;
import server.Entity.cards.spell.Spell;
import server.Entity.hero.HeroClass;
import server.data.Log;

import java.util.ArrayList;

public class SpellLogic {

    private ComplexSpell spellWantAttack = null;
    private GamePlayerLogic GPL;
    private ArrayList<Card> containDeckCard;
    private Deck deck;
    private int spellQuest;

    public SpellLogic(GamePlayerLogic gamePlayerLogic){
        this.GPL = gamePlayerLogic;
        containDeckCard = GPL.getContainDeckCard();
        deck = GPL.getDeck();
    }

    public void clickOnSpell(Card card) {
        Spell spell;
        if (card.getType().equals(Type.SPELL)) {
            Log.body(GPL.getName() + ":a spell clicked.", card.getName());
            spell = (Spell) card;
            boolean checkManaSpell = checkManaSpell(spell);
            if (checkManaSpell && GPL.isTurn()) {
                if (!spell.getName().equals("Fireball") && !spell.getName().equals("Polymorph") && !spell.getName().equals("Strength In Numbers") && !spell.getName().equals("Learn Draconic")) {
                    ((SingleSpell) spell).singleSpell(GPL);
                    Log.body("single spell will attack", spell.getName());
                    dealSpell(spell);
                }else if (spell.getName().equals("Fireball") || spell.getName().equals("Polymorph")){
                    spellWantAttack = (ComplexSpell)spell;
                }else if (spell.getName().equals("Strength In Numbers") || spell.getName().equals("Learn Draconic")){
                    dealSpell(spell);
                    ((QuestReward) spell).start(GPL);
                    GPL.setQuestReward((QuestReward) spell);
                }
                GPL.getEvent().add(spell.getName());//todo
                GPL.getMinionLogic().checkGadgetzanAuctioneer();
                GPL.getMinionLogic().checkMoargArtificer();
            }else if (!checkManaSpell){
                GPL.setErrorMassage("You don't have enough mana!");
            }else if (!GPL.isTurn()){
                GPL.setErrorMassage("It is not the turn of " + GPL.getName());
            }
        }
    }

    public void dealSpell(Spell spell) {
        Card card1 = GPL.finedCardByRef(spell);
        containDeckCard.remove(card1);
        decreaseManaSpell(card1);
    }

    private boolean checkManaSpell(Card card) {
        if (deck.getHero().getHeroClass().equals(HeroClass.Mage))
            return GPL.getMana() >= card.getMana() - GPL.getOffCardsPassive() - 2;
        else return GPL.checkMana(card);
    }

    private void decreaseManaSpell(Card card) {
        if (deck.getHero().getHeroClass().equals(HeroClass.Mage)){
           if (card.getMana() != 1 && card.getMana() != 2){
               GPL.setMana(GPL.getMana() - card.getMana() + GPL.getOffCardsPassive() + 2);
               spellQuest += card.getMana() - GPL.getOffCardsPassive() - 2;
           }
        }else {
            GPL.decreaseMana(card);
        }
    }

    public ComplexSpell getSpellWantAttack() {
        return spellWantAttack;
    }

    public void setSpellWantAttack(ComplexSpell spellWantAttack) {
        this.spellWantAttack = spellWantAttack;
    }

    public int getSpellQuest() {
        return spellQuest;
    }

    public void setSpellQuest(int spellQuest) {
        this.spellQuest = spellQuest;
    }
}
