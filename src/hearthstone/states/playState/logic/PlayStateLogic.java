package hearthstone.states.playState.logic;

import server.Entity.cards.minion.Minion;
import server.Entity.hero.HeroClass;
import server.data.Log;

public class PlayStateLogic {
    private GamePlayerLogic GPL1;
    private GamePlayerLogic GPL2;
    private static String whichMinionWantAttack;

    public PlayStateLogic(GamePlayerLogic GPL1, GamePlayerLogic GPL2) {
        this.GPL1 = GPL1;
        this.GPL2 = GPL2;
        checkIfOpponentHunter();
    }


    public static void setWhichMinionWantAttack(String whichMinionWantAttack) {
        PlayStateLogic.whichMinionWantAttack = whichMinionWantAttack;
    }

    public boolean MinionAttack(Minion minion1, Minion minion2) {
        if (whichMinionWantAttack.equals("player1") && GPL2.isTurn()) {
            Log.body("player2 want attack player1 with a minion", minion2.getName());
            if (minion2.MinionAttack(minion1, GPL2)) {
                register2register(GPL2);
                return true;
            }
        } else if (whichMinionWantAttack.equals("player2") && GPL1.isTurn()) {
            Log.body("player1 want attack player2 with a minion", minion1.getName());
            if (minion1.MinionAttack(minion2, GPL1)) {
                register2register(GPL1);
                return true;
            }
        }
        return false;
    }

    public void register2register(GamePlayerLogic GPL) {
        Minion[] minions = new Minion[7];
        for (int i = 0; i < 7; i++) {
            Minion minion = GPL.getMinionLogic().getRegister2()[i];
            if (minion != null && minion.isWindFury()) {
                if (minion.getNumberWinFury() == 1)
                    minions[i] = GPL.getMinionLogic().getRegister2()[i];
                else minion.setNumberWinFury(minion.getNumberWinFury() + 1);
            } else {
                minions[i] = GPL.getMinionLogic().getRegister1()[i];
            }
        }
        GPL.getMinionLogic().setRegister2(new Minion[7]);
        GPL.getMinionLogic().setRegister1(minions);
    }

    public void setFirstTurn() {
        GPL1.setTurn(true);
    }

    private void checkIfOpponentHunter() {
        if (GPL1.getDeck().getHero().getHeroClass().equals(HeroClass.Hunter)) {
            GPL2.getMinionLogic().setOpponentHunter(true);
        }
        if (GPL2.getDeck().getHero().getHeroClass().equals(HeroClass.Hunter)) {
            GPL1.getMinionLogic().setOpponentHunter(true);
        }

    }

}
