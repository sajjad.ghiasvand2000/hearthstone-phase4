package hearthstone.states.playState;

import server.Entity.cards.minion.Minion;
import hearthstone.states.playState.logic.GamePlayerLogic;
import hearthstone.states.playState.logic.PlayStateLogic;

public class MainMapper {
    private static MainMapper mainMapper;
    private PlayStateLogic playStateLogic;
    private GamePlayerLogic GPL1;
    private GamePlayerLogic GPL2;

    public MainMapper(GamePlayerLogic GPL1, GamePlayerLogic GPL2) {
        this.GPL1 = GPL1;
        this.GPL2 = GPL2;
        playStateLogic = new PlayStateLogic(GPL1, GPL2);
    }

    public static MainMapper getInstance(){
        return mainMapper;
    }

    public boolean MinionAttack(Minion minion1, Minion minion2){
        return playStateLogic.MinionAttack(minion1, minion2);
    }

    public void setFirstTurn(){
        playStateLogic.setFirstTurn();
    }

    public static void setMainMapper(MainMapper mainMapper) {
        MainMapper.mainMapper = mainMapper;
    }

    public void complexSpell1(){
        GPL2.getSpellLogic().getSpellWantAttack().complexSpell(GPL1, GPL1.getMinionLogic().getMinionWantAttack());
    }

    public void complexSpell2(){
        GPL1.getSpellLogic().getSpellWantAttack().complexSpell(GPL2, GPL2.getMinionLogic().getMinionWantAttack());
    }

    public static GamePlayerLogic getGPL1() {
        return getInstance().GPL1;
    }

    public static GamePlayerLogic getGPL2() {
        return getInstance().GPL2;
    }

    public boolean MinionAttackHero1(){
        return GPL1.getMinionLogic().getMinionWantAttack().MinionAttackHero(GPL2);
    }

    public boolean MinionAttackHero2(){
        return GPL2.getMinionLogic().getMinionWantAttack().MinionAttackHero(GPL1);
    }

    public void registerToRegister(GamePlayerLogic GPL){
        playStateLogic.register2register(GPL);
    }

}