package hearthstone.states.playState.graphic;

import server.Entity.cards.Card;
import server.Entity.cards.minion.Minion;
import server.Entity.cards.weapon.Weapon;
import server.data.Log;
import hearthstone.Handler;
import hearthstone.configs.PlayStateConfigs;
import hearthstone.constants.Constants;
import hearthstone.gfx.Asserts;
import hearthstone.gfx.Drawer;
import hearthstone.gfx.ImageLoader;
import hearthstone.states.collectionState.CardInitUtils;
import hearthstone.states.collectionState.CardNameInitUtils;
import hearthstone.states.collectionState.HeroNameInitUtils;
import hearthstone.states.playState.MapperPlay;
import hearthstone.ui.*;

import java.awt.*;
import java.util.ArrayList;

public class GamePlayer {
    private PlayStateConfigs p = PlayStateConfigs.getInstance();
    private MapperPlay mapperPlay;
    private Handler handler;
    private String name;
    private ArrayList<UICardImage> containLandUI;
    private ArrayList<UIDoubleImage> containDeckUI;
    private UIManager uiManagerLand;
    private UIManager uiManagerDeck;
    private UIManager uiManagerHero;
    private int controllerCounter = 0;
    private boolean controller = false;
    private Minion MinionWantSummon;
    private int helperSavingMinionTexture = 0;
    private int helperSavingHeroTexture = 0;
    private int counterPlay = 0;
    private boolean done;
    private int doneCounter;

    public GamePlayer(Handler handler, String name) {
        this.handler = handler;
        this.name = name;
        mapperPlay = new MapperPlay(handler, name);
        containDeckUI = new ArrayList<>();
        containLandUI = new ArrayList<>();
        uiManagerDeck = new UIManager(handler);
        uiManagerLand = new UIManager(handler);
        uiManagerHero = new UIManager(handler);
        cardDeckInitFirst();
        landInit();
        cardLandInitFirst();
        cardDeckInit();
        heroInit();
        heroPowerInit();
        updateHero();
        weaponInit();
    }

    public void tick() {
        if (controller) {
            controllerCounter++;
            if (controllerCounter == 20) {
                controllerCounter = 0;
                controller = false;
            }
        }
        uiManagerDeck.tick();
        uiManagerLand.tick();
        uiManagerHero.tick();
        if (mapperPlay.getUpdateFlagACardInLand() != -1) {
            System.out.println(mapperPlay.getUpdateFlagACardInLand() + " gamePlayer");
            updateHP(mapperPlay.getUpdateFlagACardInLand());
            mapperPlay.changeUpdateFlagACardInLand();
        }

        if (mapperPlay.isSpecialUpdateFlag()) {
            specialUpdate();
            mapperPlay.changeSpecialUpdateFlag();
        }

        if (mapperPlay.isUpdateFlagDeck()) {
            Log.body("update Flag Deck is true.", "");
            cardDeckInit();
            mapperPlay.changeUpdateFlagDeck();
        }

        if (mapperPlay.getExtraCard() != null) {
            cardDeckInitFirst();
        }

        if (mapperPlay.isHeroUpdateFlag()) {
            updateHero();
            mapperPlay.changeHeroUpdateFlag();
        }

        if (mapperPlay.isUpdateWeaponFlag()) {
            updateWeapon();
            mapperPlay.changeUpdateWeaponFlag();
        }

        if (mapperPlay.getQuestReward() != null) {
            if (mapperPlay.quest()) {
                mapperPlay.reward();
                mapperPlay.changeQuestReward();
                done = true;
            }
        }
    }

    public void render(Graphics2D g2D) {
        GraphicUtils.drawMana("Mana: " + mapperPlay.getMana() + "/" + mapperPlay.getLevel(), g2D, name);
        GraphicUtils.drawNumberCardInDeck("Number of cards: " + mapperPlay.getNumberOfCards(), g2D, name);
        drawPercent(g2D);
        uiManagerLand.render(g2D);
        uiManagerHero.render(g2D);
        uiManagerDeck.render(g2D);
        int height = p.get("initialYPosEvent");
        for (String s : mapperPlay.getEvent()) {
            drawer(g2D, s, height);
            height += p.get("horizontalDistanceEvent");
        }
        if (mapperPlay.getErrorMassage() != null) {
            ErrorMassagePlay(g2D, mapperPlay.getErrorMassage());
        }
    }

    private void drawPercent(Graphics2D g2D) {
        if (mapperPlay.getQuestReward() != null) {
            GraphicUtils.drawDevelopmentPercent(mapperPlay.developmentPercent(), g2D, name);
        } else if (done && doneCounter < 100) {
            GraphicUtils.drawDevelopmentPercent("done", g2D, name);
            doneCounter++;
        } else if (doneCounter == 100) {
            doneCounter = 0;
            done = false;
        }
    }

    private void cardDeckInitFirst() {
        Card card;
        for (int i = 0; i < mapperPlay.getDeckCardSize() || mapperPlay.getExtraCard() != null; i++) {
            if (i < mapperPlay.getDeckCardSize()) {
                card = mapperPlay.getDeckCard(i);
            } else {
                card = mapperPlay.getExtraCard();
            }
            Card finalCard = card;
            containDeckUI.add(new UIDoubleImage(card, mapperPlay.getDeckCardSize(), 0, 0, 0, 0, 0, p.get("widthCardBig"),
                    p.get("heightCardBig"), ImageLoader.loadDoubleImage(card.getTexturePath()[0], card.getTexturePath()[0]), () -> {
                if (controllerCounter == 0 && mapperPlay.isTurn()) {
                    mapperPlay.clickDeckCard(finalCard);
                    cardDeckInit();
                    controllerCounter++;
                    controller = true;
                }
            }));
            mapperPlay.changeExtraCard();
        }
        cardDeckInit();
    }

    public void cardDeckInitBad() {
        ArrayList<UIDoubleImage> manager;
        manager = GraphicUtils.findUIDoubleImage(containDeckUI, mapperPlay.getContainDeckCard());
        GraphicUtils.arrangeCardDeck(manager, name);
        uiManagerDeck.setButtons(CardNameInitUtils.toUIObject(manager));
    }

    public void cardDeckInit() {
        cardDeckInitBad();
        cardDeckInitBad();
    }

    public void landInit() {
        int height = p.get("maxHeightLand");
        int width = (int) (height / Constants.HEIGHT_DIVIDED_BY_WIDTH);
        float y = p.get("minInitialYPosLand" + name.charAt(6));
        float x = (float) (Constants.COMPUTER_WIDTH / 2.0 - (7 / 2.0) * (width + 2));
        for (int i = 0; i < 7; i++) {
            int finalI = i;
            containLandUI.add(new UICardImage(null, x, y, width, height, Asserts.land, () -> {
                if (containLandUI.get(finalI).getCard() == null && mapperPlay.getMinionWantedSummon() != null) {
                    summonMinion(finalI);
                } else if (containLandUI.get(finalI).getCard() != null) {
                    mapperPlay.clickLandCard(finalI);
                }
            }));
            x += width + 3;
        }
    }

    public void summonMinion(int i) {
        MinionWantSummon = mapperPlay.getMinionWantedSummon();
        containLandUI.get(i).setCard(MinionWantSummon);
        containLandUI.get(i).setImages(ImageLoader.loadDoubleImage(MinionWantSummon.getTexturePath()[0], MinionWantSummon.getTexturePath()[7]));
        mapperPlay.removeDeckMinion(MinionWantSummon, i);
        cardDeckInit();
    }

    public void updateHP(int i) {
        mapperPlay.checkHPLandCards(i);
        Minion[] minions = mapperPlay.getContainLandCard();
        if (minions[i] != null) {
            GraphicUtils.changeMinionTexture(minions[i], name, helperSavingMinionTexture);
            helperSavingMinionTexture++;
            containLandUI.get(i).setCard(minions[i]);
            if (minions[i].getMinionTexture() == null)
                containLandUI.get(i).setImages(ImageLoader.loadDoubleImage(minions[i].getTexturePath()[0], minions[i].getTexturePath()[7]));
            else
                containLandUI.get(i).setImages(ImageLoader.loadDoubleImage(minions[i].getMinionTexture()[0], minions[i].getMinionTexture()[1]));
        } else {
            containLandUI.get(i).setCard(null);
            containLandUI.get(i).setImages(Asserts.land);
        }
    }

    public void updateWeapon() {
        mapperPlay.checkDurability();
        Weapon weapon = mapperPlay.getWeapon();
        if (weapon != null) {
            GraphicUtils.changeWeaponTexture(weapon, name,1);
            if (mapperPlay.getWeapon().getWeaponTexture() == null)
                uiManagerHero.getButtons().get(2).setImages(ImageLoader.loadDoubleImage(weapon.getTexturePath()[0], weapon.getTexturePath()[7]));
            else
                uiManagerHero.getButtons().get(2).setImages(ImageLoader.loadDoubleImage(weapon.getWeaponTexture()[0], weapon.getWeaponTexture()[1]));
        } else {
            uiManagerHero.getButtons().get(2).setImages(Asserts.land);
        }
    }

    public void minionAttack() {
        updateHP(mapperPlay.getIndexMinionWantAttack());
    }

    private void specialUpdate() {
        for (int i = 0; i < 7; i++) {
            updateHP(i);
        }
    }

    public void updateHero() {
        GraphicUtils.changeHeroTexture(mapperPlay.getHero(), name, helperSavingHeroTexture);
        helperSavingHeroTexture++;
        uiManagerHero.getButtons().get(0).setImages(ImageLoader.loadDoubleImage(mapperPlay.getHero().getHeroTexture()[0], mapperPlay.getHero().getHeroTexture()[1]));
    }

    public void cardLandInitFirst() {
        uiManagerLand.setButtons(CardInitUtils.toUIObject(containLandUI));
    }

    public void heroInit() {
        uiManagerHero.addButton(new UIRecImage(p.get("initialXPosHero"), p.get("initialYPosHero" + name.charAt(6)), p.get("widthHero"), p.get("heightHero"),
                Asserts.heroesPlay[HeroNameInitUtils.recognizeHero(mapperPlay.getHeroClass()) - 1], () -> {
            mapperPlay.clickOnHero();
        }
        ));
    }

    public void heroPowerInit() {
        uiManagerHero.addButton(new UIRecImage(p.get("initialXPosHeroPower"), p.get("initialYPosHeroPower" + name.charAt(6)), p.get("widthHeroPower"), p.get("heightHeroPower"),
                Asserts.heroesHeroPowerPlay[HeroNameInitUtils.recognizeHero(mapperPlay.getHeroClass()) - 1], () -> {
            mapperPlay.heroPower();
        }
        ));
    }

    public void weaponInit() {
        uiManagerHero.addButton(new UIRecImage(p.get("initialXPosWeapon"), p.get("initialYPosWeapon" + name.charAt(6)), p.get("widthWeapon"), p.get("heightWeapon"),
                Asserts.land, () -> {
            mapperPlay.weaponAttack();
        }));
    }

//    public void updateWeapon() {
//        uiManagerHero.getButtons().get(2).setImages(ImageLoader.loadDoubleImage(mapperPlay.getWeapon().getTexturePath()[0], mapperPlay.getWeapon().getTexturePath()[7]));
//    }

    private void drawer(Graphics2D g2D, String prompt, int height) {
        Font font = new Font("Helvetica", Font.BOLD, 10);
        if (name.equals("player1"))
            g2D.setColor(Color.BLUE);
        else g2D.setColor(Color.RED);
        g2D.setFont(font);
        g2D.drawString(prompt, p.get("initialXPosEvent" + name.charAt(6)), height);
    }

    private void ErrorMassagePlay(Graphics2D g2D, String error) {
        if (mapperPlay.isError()) {
            counterPlay++;
            Drawer.Massage(error, g2D, name);
            if (counterPlay == 100) {
                mapperPlay.changeError();
                counterPlay = 0;
                mapperPlay.changeErrorMassage();
            }
        }
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public MapperPlay getMapperPlay() {
        return mapperPlay;
    }

    public String getName() {
        return name;
    }

}
