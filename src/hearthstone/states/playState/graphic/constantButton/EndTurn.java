package hearthstone.states.playState.graphic.constantButton;

import server.data.Log;
import hearthstone.Handler;
import hearthstone.states.playState.graphic.GamePlayer;
import hearthstone.ui.ClickListener;

import java.awt.*;

public class EndTurn implements ClickListener {
    private Handler handler;
    private Timer timer = null;

    public EndTurn(Handler handler) {
        this.handler = handler;
        timer = new Timer(handler);
    }

    public void render(Graphics2D g2D){
        timer.render(g2D);
    }

    @Override
    public void onClick() {
        Log.body("click end turn", "click end turn in play state");
        timer.setOnClickRequired(true);
        timer = new Timer(handler);
        timer.start();
        handler.getPlayState().getCurrentGamePlayer().getMapperPlay().clickEndTurn();
        handler.getPlayState().getCurrentGamePlayer().getMapperPlay().makeMinionsAttackable();
        handler.getPlayState().changeGamePlayer();
        GamePlayer gamePlayer = handler.getPlayState().getCurrentGamePlayer();
        gamePlayer.getMapperPlay().addCardToHand();
        gamePlayer.cardDeckInit();
    }

    public Timer getTimer() {
        return timer;
    }
}
