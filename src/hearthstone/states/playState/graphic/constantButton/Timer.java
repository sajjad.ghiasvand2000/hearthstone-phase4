package hearthstone.states.playState.graphic.constantButton;

import hearthstone.Handler;
import hearthstone.states.playState.graphic.GraphicUtils;

import java.awt.*;

public class Timer extends Thread {
    private String time = "";
    private Handler handler;
    private boolean onClickRequired = false;

    public Timer(Handler handler) {
        this.handler = handler;
    }

    public void render(Graphics2D g2D){
        GraphicUtils.drawTimer(time, g2D);
    }

    @Override
    public void run(){
        time = "";
        try {
            Thread.sleep(40000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        time = "20";
        for (int i = 0 ; i < 20 ; i++){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            time = (Integer.parseInt(time) - 1) + "";
            if (Integer.parseInt(time) == 0 && !onClickRequired){
                handler.getPlayState().getConstantButton().getEndTurn().onClick();
            }
        }
    }

    public void setOnClickRequired(boolean onClickRequired) {
        this.onClickRequired = onClickRequired;
    }
}
