package hearthstone.states.playState.graphic.constantButton;

import server.data.Log;
import hearthstone.Handler;
import hearthstone.configs.MenuStateConfigs;
import hearthstone.configs.PlayStateConfigs;
import hearthstone.gfx.Asserts;
import hearthstone.states.State;
import hearthstone.states.playState.graphic.GamePlayer;
import hearthstone.ui.UIManager;
import hearthstone.ui.UIRecImage;
import hearthstone.utils.Util;

import java.awt.*;

public class ConstantButton {
    private PlayStateConfigs p = PlayStateConfigs.getInstance();
    private static MenuStateConfigs m = MenuStateConfigs.getInstance();
    private Handler handler;
    private UIManager uiManager;
    private GamePlayer gamePlayer;
    private EndTurn endTurn;

    public ConstantButton(Handler handler) {
        this.handler = handler;
        uiManager = new UIManager(handler);
        endTurn = new EndTurn(handler);
        menu();
        Util.exit(uiManager);
        endTurn();
        endTurn.getTimer().start();
    }

    public void tick() {
        uiManager.tick();
    }

    public void render(Graphics2D g2D) {
        uiManager.render(g2D);
        endTurn.render(g2D);
    }

    private void menu() {
        uiManager.addButton(new UIRecImage(m.getMenuSateConfigs("initialXPosMenu"), m.getMenuSateConfigs("initialYPosMenu"),
                m.getMenuSateConfigs("widthMenu"), m.getMenuSateConfigs("heightMenu"), Asserts.menu, () -> {
            Log.body("click menu button", "back to menu sate");
            handler.getCollectionState().getConstantButton().setDeckReader(false);
            State.setCurrentState(handler.getMenuState());
        }));
    }

    private void endTurn() {
        uiManager.addButton(new UIRecImage(p.get("initialXPos"), p.get("initialYPos"), p.get("widthEndTurn"),
                p.get("heightEndTurn"), Asserts.endTurn, endTurn));
    }

    public EndTurn getEndTurn() {
        return endTurn;
    }
}
