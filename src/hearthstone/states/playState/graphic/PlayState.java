package hearthstone.states.playState.graphic;

import server.Entity.cards.minion.Minion;
import server.data.Log;
import hearthstone.Handler;
import hearthstone.constants.Constants;
import hearthstone.gfx.Asserts;
import hearthstone.states.State;
import hearthstone.states.playState.MainMapper;
import hearthstone.states.playState.MapperPlay;
import hearthstone.states.playState.graphic.constantButton.ConstantButton;

import java.awt.*;

public class PlayState extends State {
    private ConstantButton constantButton;
    private GamePlayer currentGamePlayer;
    private GamePlayer gamePlayer1;
    private GamePlayer gamePlayer2;
    private Minion minion1;
    private Minion minion2;
    private static MainMapper mainMapper;
    private MapperPlay map1;
    private MapperPlay map2;

    public PlayState(Handler handler) {
        super(handler);
        gamePlayer1 = new GamePlayer(handler, "player1");
        gamePlayer2 = new GamePlayer(handler, "player2");
        map1 = gamePlayer1.getMapperPlay();
        map2 = gamePlayer2.getMapperPlay();
        MainMapper.setMainMapper(new MainMapper(map1.getGamePlayerLogic(), map2.getGamePlayerLogic()));
        mainMapper = MainMapper.getInstance();
        currentGamePlayer = gamePlayer1;
        mainMapper.setFirstTurn();
        constantButton = new ConstantButton(handler);
    }

    @Override
    public void tick() {
        constantButton.tick();
        gamePlayer1.tick();
        gamePlayer2.tick();
        checkAttackMinionWithMinion();
        checkAttackSpellWithMinion();
        checkAttackMinionWithHero();
        checkAttackHeroPowerWithMinion();
        checkAttackHeroPowerWithHero();
        checkAttackWeaponWithMinion();
        endGame();
    }

    @Override
    public void render(Graphics2D g2D) {
        g2D.drawImage(Asserts.playBackground, 0, 0, Constants.COMPUTER_WIDTH, Constants.COMPUTER_HEIGHT, null);
        constantButton.render(g2D);
        gamePlayer1.render(g2D);
        gamePlayer2.render(g2D);
    }

    private void checkAttackMinionWithMinion(){
        if (map2.getMinionWantAttack() != null && map1.getMinionWantAttack() != null) {
            minion1 = map1.getMinionWantAttack();
            minion2 = map2.getMinionWantAttack();
            Log.body("attack started.", map1.getMinionWantAttack().getName() + " with " + map2.getMinionWantAttack().getName());
            MinionAttack();
        }
    }

    private void checkAttackSpellWithMinion(){
        if ((map1.getMinionWantAttack() != null && map2.getSpellWantAttack() != null)){
            mainMapper.complexSpell1();
            map2.dealSpell(map2.getSpellWantAttack());
            map1.endClickLandCard();
            map2.changeUpdateFlagDeck();
            Log.body(map2.getName() + ":spell attacked minion.", map2.getSpellWantAttack() + " & " + map1.getMinionWantAttack());
        } else if ((map2.getMinionWantAttack() != null && map1.getSpellWantAttack() != null)){
            mainMapper.complexSpell2();
            map1.dealSpell(map1.getSpellWantAttack());
            map2.endClickLandCard();
            map1.changeUpdateFlagDeck();
            Log.body(map1.getName() + ":spell attacked minion.", map1.getSpellWantAttack() + " & " + map2.getMinionWantAttack());

        }
    }

    private void checkAttackMinionWithHero(){
        if (map1.getMinionWantAttack() != null && map2.isHeroAttack()){
            if (mainMapper.MinionAttackHero1()){
                mainMapper.registerToRegister(map1.getGamePlayerLogic());
                gamePlayer1.updateHP(map1.getIndexMinionWantAttack());
                map1.endClickLandCard();
                gamePlayer2.updateHero();
                map2.changeHeroAttack();
                Log.body(map1.getName() + ":minion attacked hero.", map1.getSpellWantAttack() + " & " + map2.getHero());
            }
        }else if (map2.getMinionWantAttack() != null && map1.isHeroAttack()){
            if (mainMapper.MinionAttackHero2()){
                mainMapper.registerToRegister(map2.getGamePlayerLogic());
                gamePlayer2.updateHP(map2.getIndexMinionWantAttack());
                map2.endClickLandCard();
                gamePlayer1.updateHero();
                map1.changeHeroAttack();
                Log.body(map2.getName() + ":minion attacked hero.", map2.getSpellWantAttack() + " & " + map1.getHero());

            }
        }
    }

    private void checkAttackHeroPowerWithMinion(){
        if (map1.isHeroPowerWantAttack() && map2.getMinionWantAttack() != null){
            if (map1.heroPowerAttackMinion(map2.getMinionWantAttack())) {
                gamePlayer2.updateHP(map2.getIndexMinionWantAttack());
                map2.endClickLandCard();
                map1.changeHeroPowerWantAttack();
                Log.body(map1.getName() + ":hero power attacked minion.", map1.getHero() + " & " + map2.getMinionWantAttack());
            }
        }else if (map2.isHeroPowerWantAttack() && map1.getMinionWantAttack() != null){
            if(map2.heroPowerAttackMinion(map1.getMinionWantAttack())) {
                gamePlayer1.updateHP(map1.getIndexMinionWantAttack());
                map1.endClickLandCard();
                map2.changeHeroPowerWantAttack();
                Log.body(map2.getName() + ":hero power attacked minion.", map2.getHero() + " & " + map1.getMinionWantAttack());
            }
        }
    }

    private void checkAttackHeroPowerWithHero(){
        if (map1.isHeroPowerWantAttack() && map2.isHeroAttack()){
            if (map1.heroPowerAttackHero(map2.getHero())){
                gamePlayer2.updateHero();
                map2.changeHeroAttack();
                map1.changeHeroPowerWantAttack();
                Log.body(map1.getName() + ":hero power attacked hero.", map1.getHero() + " & " + map2.getHero());
            }
        }else if (map2.isHeroPowerWantAttack() && map1.isHeroAttack()){
            if (map2.heroPowerAttackHero(map1.getHero())){
                gamePlayer1.updateHero();
                map1.changeHeroAttack();
                map2.changeHeroPowerWantAttack();
                Log.body(map2.getName() + ":hero power attacked hero.", map2.getHero() + " & " + map1.getHero());
            }
        }
    }

    private void checkAttackWeaponWithMinion(){
        if (map1.getWeapon() != null && map1.isWeaponWantAttack() && map2.getMinionWantAttack() != null){
            map1.getWeapon().weaponAttackMinion(map2.getMinionWantAttack());
            map2.endClickLandCard();
            map1.changeWeaponWantAttack();
            gamePlayer2.updateHP(map2.getIndexMinionWantAttack());
            gamePlayer1.updateWeapon();
            Log.body(map1.getName() + ":weapon attacked minion.", map1.getWeapon() + " & " + map2.getMinionWantAttack());
        } else if (map2.getWeapon() != null && map2.isWeaponWantAttack() && map1.getMinionWantAttack() != null){
            map2.getWeapon().weaponAttackMinion(map1.getMinionWantAttack());
            map1.endClickLandCard();
            map2.changeWeaponWantAttack();
            gamePlayer1.updateHP(map1.getIndexMinionWantAttack());
            gamePlayer2.updateWeapon();
            Log.body(map2.getName() + ":weapon attacked minion.", map2.getWeapon() + " & " + map1.getMinionWantAttack());

        }
    }

    private void endGame(){
        if (map1.getDeck().getHero().getHP() < 1 || map2.getDeck().getHero().getHP() < 1){
            State.setCurrentState(handler.getMenuState());
        }
    }

    public void changeGamePlayer() {
        if (currentGamePlayer.getName().equals("player1")) {
            map1.changeTurn("player1");
            map2.changeTurn("player1");
            currentGamePlayer = gamePlayer2;
        } else {
            map1.changeTurn("player2");
            map2.changeTurn("player2");
            currentGamePlayer = gamePlayer1;
        }
    }

    public void MinionAttack() {
        if (mainMapper.MinionAttack(minion1, minion2)) {
            gamePlayer1.minionAttack();
            gamePlayer2.minionAttack();
        }
        map1.endClickLandCard();
        map2.endClickLandCard();
    }

    public GamePlayer getCurrentGamePlayer() {
        return currentGamePlayer;
    }

    public ConstantButton getConstantButton() {
        return constantButton;
    }
}