package hearthstone.states.playState.graphic;

import server.Entity.cards.weapon.Weapon;
import server.Entity.hero.Hero;
import server.Entity.cards.Card;
import server.Entity.cards.minion.Minion;
import hearthstone.configs.PlayStateConfigs;
import hearthstone.constants.Constants;
import hearthstone.gfx.ImageLoader;
import hearthstone.ui.UIDoubleImage;

import java.awt.*;
import java.util.ArrayList;

public class GraphicUtils {
    private static PlayStateConfigs p = PlayStateConfigs.getInstance();

    public static ArrayList<UIDoubleImage> findUIDoubleImage(ArrayList<UIDoubleImage> uiDoubleImages, ArrayList<Card> cards) {
        ArrayList<UIDoubleImage> uiObjects = new ArrayList<>(uiDoubleImages);
        ArrayList<UIDoubleImage> manager = new ArrayList<>();
        for (Card card : cards) {
            for (UIDoubleImage uiDoubleImage : uiObjects) {
                if (card.equals(uiDoubleImage.getCard())) {
                    manager.add(uiDoubleImage);
                    uiObjects.remove(uiDoubleImage);
                    break;
                }
            }
        }
        return manager;
    }

    public static void changeMinionTexture(Minion minion, String name, int i) {
            ImageLoader.writeOnMinionImage(ImageLoader.loadImage("/texture/Minion/" + minion.getName() + "1.png"),
                    minion, Integer.toString(minion.getHP()), Integer.toString(minion.getAttack()), name, i, 1, "Minions");
            ImageLoader.writeOnMinionImage(ImageLoader.loadImage("/texture/Minion/" + minion.getName() + "2.png"),
                    minion, Integer.toString(minion.getHP()), Integer.toString(minion.getAttack()), name, i, 2, "Minions");
            String[] s = new String[]{"/texture/Minions during play/" + minion.getName() + "1_" + name + "_" + i + ".png"
                    , "/texture/Minions during play/" + minion.getName() + "2_" + name + "_" + i + ".png"};
            minion.setMinionTexture(s);
    }

    public static void changeWeaponTexture(Weapon weapon, String name, int i) {
        ImageLoader.writeOnMinionImage(ImageLoader.loadImage("/texture/weapon/" + weapon.getName() + "1.png"),
                weapon, Integer.toString(weapon.getDurability()), Integer.toString(weapon.getAttack()), name, i, 1, "weapons");
        ImageLoader.writeOnMinionImage(ImageLoader.loadImage("/texture/weapon/" + weapon.getName() + "2.png"),
                weapon, Integer.toString(weapon.getDurability()), Integer.toString(weapon.getAttack()), name, i, 2, "weapons");
        String[] s = new String[]{"/texture/weapons during play/" + weapon.getName() + "1_" + name + "_" + i + ".png"
                , "/texture/weapons during play/" + weapon.getName() + "2_" + name + "_" + i + ".png"};
        weapon.setWeaponTexture(s);
    }

    public static void changeHeroTexture(Hero hero, String name, int i) {
        ImageLoader.writeOnHeroImage(ImageLoader.loadImage("/texture/heroes/" + hero.getHeroClass().toString() + "1.png"),
                hero, Integer.toString(hero.getHP()), name, i, 1);
        ImageLoader.writeOnHeroImage(ImageLoader.loadImage("/texture/heroes/" + hero.getHeroClass().toString() + "2.png"),
                hero, Integer.toString(hero.getHP()), name, i, 2);
        String[] s = new String[]{"/texture/heroes during play/" + hero.getHeroClass().toString() + "1_" + name + "_" + i + ".png"
                , "/texture/heroes during play/" + hero.getHeroClass().toString() + "2_" + name + "_" + i + ".png"};
        hero.setHeroTexture(s);
    }

    public static void arrangeCardDeck(ArrayList<UIDoubleImage> uiDoubleImages, String n) {
        int cardWidth = 0;
        if (uiDoubleImages.size() != 0)
            cardWidth = p.get("maxWidthDeck") / Math.min(uiDoubleImages.size(), 6) - (3 * uiDoubleImages.size() + 1);
        cardWidth = Math.min(cardWidth, p.get("maxWidthCard"));
        int height = (int) (cardWidth * Constants.HEIGHT_DIVIDED_BY_WIDTH);
        float y = (float) ((p.get("maxHeightDeck") - height) / 2.0 + p.get("minInitialYPos" + n.charAt(6)));
        float x = (float) (Constants.COMPUTER_WIDTH / 2.0 - (uiDoubleImages.size() / 2.0) * (cardWidth + 2));
        float x2 = (float) (Constants.COMPUTER_WIDTH / 2.0 - (uiDoubleImages.size() / 2.0) * (cardWidth + 2) - cardWidth / 2);
        float y2;
        if (n.equals("player1"))
            y2 = y - p.get("heightCardBig");
        else y2 = y + height - 10;
        for (UIDoubleImage uiDoubleImage : uiDoubleImages) {
            uiDoubleImage.setX(x);
            uiDoubleImage.setY(y);
            uiDoubleImage.setX2(x2);
            uiDoubleImage.setY2(y2);
            uiDoubleImage.setWidth(cardWidth);
            uiDoubleImage.setHeight(height);
            x += cardWidth + 5;
            x2 += cardWidth + 5;
        }
    }


    public static void drawMana(String prompt, Graphics2D g2D, String name) {
        Font font = new Font("Helvetica", Font.BOLD, 20);
        if (name.equals("player1"))
            g2D.setColor(Color.BLUE);
        else g2D.setColor(Color.RED);
        g2D.setFont(font);
        g2D.drawString(prompt, p.get("initialXPosMana"), p.get("initialYPosMana" + name.charAt(6)));
    }

    public static void drawNumberCardInDeck(String prompt, Graphics2D g2D, String name) {
        Font font = new Font("Helvetica", Font.BOLD, 20);
        if (name.equals("player1"))
            g2D.setColor(Color.BLUE);
        else g2D.setColor(Color.RED);
        g2D.setFont(font);
        g2D.drawString(prompt, p.get("initialXPosNumberCardInDeck"), p.get("initialYPosNumberCardInDeck" + name.charAt(6)));
    }

    public static void drawTimer(String timer, Graphics2D g2D){
        Font font = new Font("Helvetica", Font.BOLD, 50);
        g2D.setColor(Color.GREEN);
        g2D.setFont(font);
        g2D.drawString(timer, p.get("initialXPosTimer"), p.get("initialYPosTimer"));
    }

    public static void drawDevelopmentPercent(String percent, Graphics2D g2D, String name){
        Font font = new Font("Helvetica", Font.BOLD, 50);
        g2D.setColor(Color.ORANGE);
        g2D.setFont(font);
        g2D.drawString(percent, p.get("initialXPosDevelopmentPercent"), p.get("initialYPosDevelopmentPercent"+ name.charAt(6)));
    }
}
