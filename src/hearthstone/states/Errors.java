package hearthstone.states;

import hearthstone.gfx.Drawer;
import hearthstone.states.collectionState.CollectionState;
import hearthstone.states.shopState.ShopState;


import java.awt.*;

public class Errors {
    private static int counterCollection = 0;
    private static int counterShop = 0;

    public static void ErrorMassageCollection(Graphics2D g2D, String error){
        if (CollectionState.isError()) {
            counterCollection++;
            Drawer.Massage(error, g2D, "");
            if (counterCollection == 100){
                CollectionState.setError(false);
                counterCollection = 0;
                CollectionState.setErrorMassage(null);
            }
        }
    }

    public static void ErrorMassageShop(Graphics2D g2D, String error){
        if (ShopState.isError()) {
           counterShop++;
            Drawer.Massage(error, g2D, "");
            if (counterShop == 100){
                ShopState.setError(false);
                counterShop = 0;
                ShopState.setErrorMassage(null);
            }
        }
    }
}
