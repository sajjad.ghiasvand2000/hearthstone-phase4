package hearthstone.states.shopState;

import hearthstone.Handler;
import hearthstone.configs.CollectionStateConfigs;
import hearthstone.configs.MenuStateConfigs;
import hearthstone.gfx.Asserts;
import hearthstone.states.State;
import hearthstone.ui.UIManager;
import hearthstone.ui.UIRecImage;
import hearthstone.utils.Util;

import java.awt.*;
import java.io.IOException;

public class ConstantButton {
    private static CollectionStateConfigs c = CollectionStateConfigs.getInstance();
    private static MenuStateConfigs m = MenuStateConfigs.getInstance();
    private Handler handler;
    private UIManager uiManager;

    public ConstantButton(Handler handler) {
        this.handler = handler;
        uiManager = new UIManager(handler);
        nextPageButton();
        previousPageButton();
        sell();
        buy();
        menu();
        Util.exit(uiManager);
    }

    public void render(Graphics2D g2D) {
        uiManager.render(g2D);
    }

    public void tick() {
        uiManager.tick();
    }

    private void nextPageButton() {
        uiManager.addButton(new UIRecImage(1450, 380, 20, 20, Asserts.nextPage, () -> {
            if (handler.getShopState().getPage() == 1) {
                handler.getShopState().setPage(2);
            }
            handler.getShopState().cardInit();
        }));
    }

    private void previousPageButton() {
        uiManager.addButton(new UIRecImage(10, 380, 20, 20, Asserts.nextPage, () -> {
            if (handler.getShopState().getPage() == 2) {
                handler.getShopState().setPage(1);
            }
            handler.getShopState().cardInit();
        }));
    }

    private void buy() {
        uiManager.addButton(new UIRecImage(c.get("initialXPosLockButton"), c.get("initialYPosLockButton"),
                c.get("widthLockButton"), c.get("heightLockButton"), Asserts.lock, () -> {
            handler.getShopState().setShowLockCards(true);
            handler.getShopState().setShowUnLockCards(false);
            handler.getShopState().cardInit();
        }));

    }

    private void sell() {
        uiManager.addButton(new UIRecImage(c.get("initialXPosLockButton") + c.get("widthLockButton") +
                c.get("horizontalDistanceLockButton"), c.get("initialYPosLockButton"),
                c.get("widthLockButton"), c.get("heightLockButton"), Asserts.unlock, () -> {
            handler.getShopState().setShowLockCards(false);
            handler.getShopState().setShowUnLockCards(true);
            handler.getShopState().cardInit();
        }));
    }

    private void menu(){
        uiManager.addButton(new UIRecImage(m.getMenuSateConfigs("initialXPosMenu"), m.getMenuSateConfigs("initialYPosMenu"),
                m.getMenuSateConfigs("widthMenu"), m.getMenuSateConfigs("heightMenu"), Asserts.menu, () -> {
            State.setCurrentState(handler.getMenuState());
            handler.getCollectionState().updateCardInit();
        }) );
    }
}
