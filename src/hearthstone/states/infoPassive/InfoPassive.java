package hearthstone.states.infoPassive;

import server.data.Log;
import hearthstone.Handler;
import hearthstone.configs.PassiveStateConfigs;
import hearthstone.constants.Constants;
import hearthstone.gfx.Asserts;
import hearthstone.states.State;
import hearthstone.states.changeCardState.ChangeCardState;
import hearthstone.ui.UIManager;
import hearthstone.ui.UIRecImage;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

public class InfoPassive extends State {
    private PassiveStateConfigs p = PassiveStateConfigs.getInstance();
    private ArrayList<UIRecImage> containPassive;
    private UIManager uiManager;
    private boolean twiceDrawPassive = false;
    private int offCardPassive = 0;
    private int ManaJumpPassive = 0;
    private int freePower = 0;
    private boolean nurse;

    public InfoPassive(Handler handler) {
        super(handler);
        uiManager = new UIManager(handler);
        containPassive = new ArrayList<>();
        twiceDraw();
        offCard();
        ManaJump();
        warriors();
        freePower();
        passiveInit();
    }

    @Override
    public void tick() {
        uiManager.tick();
    }

    @Override
    public void render(Graphics2D g2D) {
        g2D.drawImage(Asserts.passiveBackground, 0, 0, Constants.COMPUTER_WIDTH, Constants.COMPUTER_HEIGHT, null);
        uiManager.render(g2D);
    }

    private void twiceDraw() {
        containPassive.add(new UIRecImage(0, 0, p.get("widthCard"), p.get("heightCard"), Asserts.passive[1], () -> {
            Log.body("info passive", "click twice draw");
            twiceDrawPassive = true;
            checkNextState();
        }));
    }

    private void offCard() {
        containPassive.add(new UIRecImage(0, 0, p.get("widthCard"), p.get("heightCard"), Asserts.passive[2], () -> {
            Log.body("info passive", "click off card");
            offCardPassive = 1;
            checkNextState();
        }));
    }

    private void ManaJump() {
        containPassive.add(new UIRecImage(0, 0, p.get("widthCard"), p.get("heightCard"), Asserts.passive[0], () -> {
            Log.body("info passive", "click manaJump");
            ManaJumpPassive = 1;
            checkNextState();
        }));
    }

    private void warriors() {
        containPassive.add(new UIRecImage(0, 0, p.get("widthCard"), p.get("heightCard"), Asserts.passive[4], () -> {
            Log.body("info passive", "click warriors");
            nurse = true;
            checkNextState();
        }));
    }

    private void freePower() {
        containPassive.add(new UIRecImage(0, 0, p.get("widthCard"), p.get("heightCard"), Asserts.passive[3], () -> {
            Log.body("info passive", "click free power");
            freePower = 1;
            checkNextState();
        }));
    }

    private void start() {
        ChangeCardState changeCardState = new ChangeCardState(handler);
        handler.setChangeCardState1(changeCardState);
        State.setCurrentState(changeCardState);
    }

    private void secondInfoPassive(){
        InfoPassive infoPassive = new InfoPassive(handler);
        handler.setInfoPassive2(infoPassive);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        State.setCurrentState(infoPassive);
    }

    private void checkNextState(){
        if (!Utils.getInfoNumber()){
            Utils.setInfoNumber(true);
            secondInfoPassive();
        }else {
            Utils.setInfoNumber(false);
            start();
        }
    }

    private void passiveInit() {
        Collections.shuffle(containPassive);
        ArrayList<UIRecImage> manger = new ArrayList<>();
        Collections.addAll(manger, containPassive.get(0), containPassive.get(1), containPassive.get(2));
        Utils.arrange(manger);
        uiManager.setButtons(Utils.toUIObject(manger));
    }

    public boolean isTwiceDrawPassive() {
        return twiceDrawPassive;
    }

    public int getOffCardPassive() {
        return offCardPassive;
    }

    public int getManaJumpPassive() {
        return ManaJumpPassive;
    }

    public int getFreePower() {
        return freePower;
    }

    public boolean isNurse() {
        return nurse;
    }
}
