package hearthstone.states.collectionState;

import server.Entity.hero.HeroClass;
import hearthstone.configs.CollectionStateConfigs;
import hearthstone.gfx.ImageLoader;
import hearthstone.ui.UICardImage;
import hearthstone.ui.UIObject;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

public class CardInitUtils {
    private static CollectionStateConfigs c = CollectionStateConfigs.getInstance();

    public static ArrayList<UICardImage> manaFilter(int mana, ArrayList<UICardImage> uiCardImages) {
        if (mana == -1) return uiCardImages;
        ArrayList<UICardImage> manager = new ArrayList<>();
        for (UICardImage uiCardImage : uiCardImages) {
            if (uiCardImage.getCard().getMana() == mana) {
                manager.add(uiCardImage);
            }
        }
        return manager;
    }

    public static ArrayList<UICardImage> searchFilter(String input, ArrayList<UICardImage> uiCardImages){
        if (input.equals("")) return uiCardImages;
        else {
            ArrayList<UICardImage> manager = new ArrayList<>();
            for (UICardImage uiCardImage : uiCardImages) {
                if (uiCardImage.getCard().getName().toLowerCase().contains(input.toLowerCase()))
                    manager.add(uiCardImage);
            }
            return manager;
        }
    }

    public static ArrayList<UICardImage> heroFilter(HeroClass heroClass, ArrayList<UICardImage> uiCardImages) {
        ArrayList<UICardImage> manager = new ArrayList<>();
        for (UICardImage uiCardImage : uiCardImages) {
            if (uiCardImage.getCard().getHeroClass().equals(heroClass)) {
                manager.add(uiCardImage);
            }
        }
        return manager;
    }

    public static ArrayList<UICardImage> lockFilter(boolean showUnLockCards, boolean showLockCards, ArrayList<UICardImage> uiCardImages) {
        ArrayList<UICardImage> manager = new ArrayList<>();
        if (!showLockCards && !showUnLockCards) return uiCardImages;
        else if (showLockCards){
            for (UICardImage uiCardImage : uiCardImages) {
                if (uiCardImage.getCard().isLock()) {
                    manager.add(uiCardImage);
                }
            }
        }else {
            for (UICardImage uiCardImage : uiCardImages) {
                if (!uiCardImage.getCard().isLock()) {
                    manager.add(uiCardImage);
                }
            }
        }
        return manager;
    }

    public static void arrangeCards(ArrayList<UICardImage> uiCardImages) {
        int counter = 0;
        float xPosCard = c.get("initialXPosCard");
        float yPosCard = c.get("initialYPosCard");
        for (UICardImage uiCardImage : uiCardImages) {
            counter++;
            uiCardImage.setX(xPosCard);
            uiCardImage.setY(yPosCard);
            xPosCard += c.get("widthCard") + c.get("horizontalDistanceCard");
            if (counter == c.get("maxCardRow")) {
                yPosCard += c.get("heightCard") + c.get("verticalDistanceCard");
                xPosCard -= (c.get("widthCard") + c.get("horizontalDistanceCard")) * c.get("maxCardRow");
            } else if (counter == c.get("maxCardRow") * c.get("maxCardColumn")) {
                xPosCard = c.get("initialXPosCard");
                yPosCard = c.get("initialYPosCard");
                counter = 0;
            }
        }
    }

    public static ArrayList<UICardImage> amountFilter(int page, ArrayList<UICardImage> uiCardImages){
        ArrayList<UICardImage> manager = new ArrayList<>();
        if (uiCardImages.size() <= 10)
            return uiCardImages;
        else if (uiCardImages.size() <= 20){
            if (page == 1) {
                for (int i = 0; i < 10; i++) {
                    manager.add(uiCardImages.get(i));
                }
            }else if (page == 2){
                for (int i = 10; i < uiCardImages.size(); i++) {
                    manager.add(uiCardImages.get(i));
                }
            }
            return manager;
        }else if (uiCardImages.size() <= 30){
            if (page == 1) {
                for (int i = 0; i < 10; i++) {
                    manager.add(uiCardImages.get(i));
                }
            }else if (page == 2){
                for (int i = 10; i < 20; i++) {
                    manager.add(uiCardImages.get(i));
                }
            }else if (page == 3){
                for (int i = 20; i < uiCardImages.size(); i++) {
                    manager.add(uiCardImages.get(i));
                }
            }
            return manager;
        }
            else return null;//todo
    }

    public static void setTexture(UICardImage uiCardImage, int frequency) throws IOException {
        BufferedImage[] bufferedImages = new BufferedImage[2];
        if (frequency == 0)
            bufferedImages = ImageLoader.loadDoubleImage(uiCardImage.getCard().getTexturePath()[2], uiCardImage.getCard().getTexturePath()[5]);
        else if (frequency == 1)
            bufferedImages = ImageLoader.loadDoubleImage(uiCardImage.getCard().getTexturePath()[1], uiCardImage.getCard().getTexturePath()[4]);
        else if (frequency == 2)
            bufferedImages = ImageLoader.loadDoubleImage(uiCardImage.getCard().getTexturePath()[3], uiCardImage.getCard().getTexturePath()[6]);
        uiCardImage.setImages(bufferedImages);
    }

    public static ArrayList<UIObject> toUIObject(ArrayList<UICardImage> uiCardImages){
        ArrayList<UIObject> uiObjects = new ArrayList<>(uiCardImages);
        return uiObjects;
    }
}
