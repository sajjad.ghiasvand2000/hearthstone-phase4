package hearthstone.states.collectionState;

import server.Entity.cards.Card;
import hearthstone.configs.CollectionStateConfigs;
import hearthstone.gfx.ImageLoader;
import hearthstone.ui.UIDoubleImage;
import hearthstone.ui.UIObject;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class CardNameInitUtils {
    private static CollectionStateConfigs c = CollectionStateConfigs.getInstance();

    public static void setTexture(UIDoubleImage uiDoubleImage, int frequency) {
        BufferedImage[] bufferedImages = new BufferedImage[2];
        Card card = uiDoubleImage.getCard();
        if (frequency == 1) {
            bufferedImages = ImageLoader.loadDoubleImage(card.getTexturePath()[8], card.getTexturePath()[0]);
        } else if (frequency == 2) {
            bufferedImages = ImageLoader.loadDoubleImage(card.getTexturePath()[9], card.getTexturePath()[0]);
        }
        uiDoubleImage.setImages(bufferedImages);
    }

    public static void arrangeCardName(ArrayList<UIDoubleImage> uiDoubleImages){
        for (int i = 0 ; i < uiDoubleImages.size() ;i++){
            UIDoubleImage uiDoubleImage = uiDoubleImages.get(i);
            float y = c.get("initialYPosCardName")+ i * c.get("heightCardName") +
                    (i + 1) * c.get("verticalDistanceCardName");
            uiDoubleImage.setY(y);
            uiDoubleImage.setY2((float) (y - c.get("heightCardName") / 2.0));
        }
    }
    public static ArrayList<UIObject> toUIObject(ArrayList<UIDoubleImage> uiDoubleImages){
        ArrayList<UIObject> uiObjects = new ArrayList<>(uiDoubleImages);
        return uiObjects;
    }
}
