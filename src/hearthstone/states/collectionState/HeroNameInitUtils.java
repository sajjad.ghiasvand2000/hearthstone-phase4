package hearthstone.states.collectionState;

import server.Entity.hero.HeroClass;
import server.Entity.cards.Card;
import server.Entity.Deck;
import hearthstone.ui.UIDoubleImage;
import hearthstone.ui.UIObject;

import java.util.ArrayList;

public class HeroNameInitUtils {

    public static HeroClass recognizeHero(int i){
        if (i == 0)
            return HeroClass.Mage;
        else if(i == 1)
            return HeroClass.Warlock;
        else if (i == 2)
            return HeroClass.Rogue;
        else if (i == 3)
            return HeroClass.Paladin;
        else if (i == 4)
            return HeroClass.Hunter;
        return null;
    }

    public static int recognizeHero(HeroClass heroClass){
        if (heroClass.equals(HeroClass.Mage))
            return 1;
        else if(heroClass.equals(HeroClass.Warlock))
            return 2;
        else if (heroClass.equals(HeroClass.Rogue))
            return 3;
        else if (heroClass.equals(HeroClass.Paladin))
            return 4;
        else if (heroClass.equals(HeroClass.Hunter))
            return 5;
        return 0;
    }
    public static ArrayList<UIObject> toUIObject(ArrayList<UIDoubleImage> uiDoubleImages){
        ArrayList<UIObject> uiObjects = new ArrayList<>(uiDoubleImages);
        return uiObjects;
    }

    public static boolean canChangeHero(Deck deck){
        for (Card card : deck.getCards()) {
            if (card.getHeroClass().equals(deck.getHero()))
                return false;
        }
        return true;
    }
}
