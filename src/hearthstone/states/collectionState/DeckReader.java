package hearthstone.states.collectionState;

import server.Entity.Deck;
import server.Entity.EntityUtils;
import server.Entity.MainPlayer;
import server.Entity.cards.Card;
import server.Entity.hero.HeroClass;
import com.google.gson.Gson;
import hearthstone.utils.Util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DeckReader {
    private ArrayList<String> friend;
    private ArrayList<String> enemy;
    private DeckReader deckReader;
    private Deck deck1;
    private Deck deck2;


    public DeckReader(Deck deck1, Deck deck2) {
        friend = new ArrayList<>();
        enemy = new ArrayList<>();
        this.deck1 = deck1;
        this.deck2 = deck2;
        convert();
        setCards();
        setHero();
    }


    private void convert() {
        Scanner fileReader = null;
        try {
            fileReader = new Scanner(new FileReader("deckReader.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        deckReader = new Gson().fromJson(fileReader.nextLine(), DeckReader.class);
    }

    public void setCards(){
        List<Card> cards1 = new ArrayList<>();
        List<Card> cards2 = new ArrayList<>();
        for (String s : deckReader.friend) {
            cards1.add(Util.findCardFromMainPlayer(s));
        }
        for (String s : deckReader.enemy) {
            if (s.contains("reward")){
                EntityUtils.setReward(s.substring(28));
                s = s.substring(0, 19);
            }
            cards2.add(Util.findCardFromMainPlayer(s));
        }
        deck1.setCards(cards1);
        deck2.setCards(cards2);
    }

    private void setHero(){
        if (!deck1.getHero().getHeroClass().equals(HeroClass.Mage)){
            deck1.setHero(MainPlayer.getInstance().getHeroes().get(0));
        }
        if (!deck2.getHero().getHeroClass().equals(HeroClass.Mage)){
            deck2.setHero(MainPlayer.getInstance().getHeroes().get(0));
        }
    }

    public Deck getDeck1() {
        return deck1;
    }

    public Deck getDeck2() {
        return deck2;
    }

    //
//    public void write(){
//        try {
//            MyFile.fileWriter("salam.txt", makeDeckReader());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    private DeckReader makeDeckReader(){
//        DeckReader deckReader = new DeckReader();
//        Collections.addAll(deckReader.friend, "Learn Draconic","book of specters","minion","minion","minion",
//                "spell","spell","spell","friendly smith","curio collector","pharaoh's Blessing","Polymorph","sprint",
//                "special hero card","special hero card","minion","minion","minion","minion","minion","minion","minion","minion" );
//        Collections.addAll(deckReader.enemy, "Strength in Numbers->reward: Security Rover","Dreadscale","weapon"
//                ,"swarm of locusts","tomb warden","sathrovarr","minion","minion","minion","minion","minion","Security Rover");
//        return deckReader;
//    }

}
