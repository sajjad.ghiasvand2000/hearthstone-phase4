package hearthstone.states.changeCardState;

import server.Entity.Deck;
import server.Entity.cards.Card;
import hearthstone.Handler;
import hearthstone.configs.PassiveStateConfigs;
import hearthstone.constants.Constants;
import hearthstone.gfx.Asserts;
import hearthstone.gfx.ImageLoader;
import hearthstone.states.State;
import hearthstone.states.collectionState.CardInitUtils;
import hearthstone.states.playState.graphic.PlayState;
import hearthstone.ui.UICardImage;
import hearthstone.ui.UIManager;
import hearthstone.ui.UIRecImage;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChangeCardState extends State {

    private PassiveStateConfigs p = PassiveStateConfigs.getInstance();
    private Deck deck;
    private UIManager uiManager;
    private ArrayList<UICardImage> uiCardImages;
    private boolean[] change;

    public ChangeCardState(Handler handler) {
        super(handler);
        if (!Utils.isChangeCardNumber()){
            deck = handler.getCollectionState().getPlayDeck("player1");
        }else {
            deck = handler.getCollectionState().getPlayDeck("player2");
        }
        change = new boolean[3];
        if (!handler.getCollectionState().getConstantButton().isDeckReader())
            Collections.shuffle(deck.getCards());
        uiManager = new UIManager(handler);
        uiCardImages = new ArrayList<>();
        cards();
        uiManager.setButtons(CardInitUtils.toUIObject(uiCardImages));
        done();
    }

    @Override
    public void tick() {
        uiManager.tick();
    }

    @Override
    public void render(Graphics2D g2D) {
        g2D.drawImage(Asserts.passiveBackground, 0, 0, Constants.COMPUTER_WIDTH, Constants.COMPUTER_HEIGHT, null);
        uiManager.render(g2D);
    }

    private void cards(){
        int x = p.get("initialXPosCard");
        int y = p.get("initialYPosCard");
        for (int i = 0 ; i < 3 ; i++) {
            Card card = deck.getCards().get(i);
            int finalI = i;
            uiCardImages.add(new UICardImage(card, x, y, p.get("widthCard"), p.get("heightCard"),
                    ImageLoader.loadDoubleImage(card.getTexturePath()[0], card.getTexturePath()[7]), () -> {
                change[finalI] = true;
                    }));
            x += p.get("widthCard") + p.get("horizontalDistanceCard");
        }
    }

    private void done(){
        uiManager.addButton(new UIRecImage(p.get("changeCardinitialXPos"), p.get("changeCardinitialYPos"), p.get("changeCardDoneWidth"),
                p.get("changeCardDoneOkHeight"), Asserts.doneWriteHeroName, () -> {
                    changeCard();
                    checkNextState();
                }));
    }

    private void checkNextState(){
        if (!Utils.isChangeCardNumber()){
            Utils.setChangeCardNumber(true);
            secondChangeCard();
        }else {
            Utils.setChangeCardNumber(false);
            start();
        }
    }

    private void secondChangeCard(){
        ChangeCardState changeCardState = new ChangeCardState(handler);
        handler.setChangeCardState2(changeCardState);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        State.setCurrentState(changeCardState);
    }

    private void start(){
        PlayState playState = new PlayState(handler);
        handler.setPlayState(playState);
        State.setCurrentState(playState);
    }

    private void changeCard(){
        List<Card> cards = new ArrayList<>(deck.getCards());
        Card help;
        for (int i = 0 ; i < 3 ; i++){
            if (change[i]){
                help = cards.get(i);
                cards.set(i, cards.get(i+3));
                cards.set(i+3, help);
            }
        }
        deck.setCards(cards);
    }
}
