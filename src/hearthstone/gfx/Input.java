package hearthstone.gfx;

import hearthstone.Handler;
import hearthstone.configs.CollectionStateConfigs;

import java.awt.*;

public class Input {
    private CollectionStateConfigs c = CollectionStateConfigs.getInstance();
    private Handler handler;
    private String input = "";
    private int controllerCounter = 0;
    private boolean controller = false;
    private int number;
    private int x;
    private int y;
    private int fontSize;

    public Input(Handler handler, int number, int x, int y, int fontSize) {
        this.handler = handler;
        this.number = number;
        this.x = x;
        this.y = y;
        this.fontSize = fontSize;

    }

    public void tick() {
        if (controller) {
            controllerCounter++;
            if (controllerCounter == 8) {
                controllerCounter = 0;
                controller = false;
            }
        }
        if (controllerCounter == 0) {
            for (int i = 0; i < 256; i++) {
                if (handler.getKeyManager().getKeys()[8] && !input.equals("")) {
                    input = input.substring(0, input.length() - 1);
                } else if (handler.getKeyManager().getKeys()[i] && input.length() < number) {
                    input += (char) i;
                    input = input.toLowerCase();
                }
            }
            controllerCounter++;
            controller = true;
        }
    }

    public void render(Graphics2D g2D) {
        drawString(g2D);
    }

    public void drawString(Graphics2D g2D) {
        Font font = new Font(input, Font.BOLD, fontSize);
        g2D.setColor(Color.BLACK);
        g2D.setFont(font);
        g2D.drawString(input, x, y);
    }

    public String getInput() {
        return input;
    }
}
