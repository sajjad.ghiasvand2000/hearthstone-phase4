package hearthstone.gfx;

import java.awt.image.BufferedImage;

public class Asserts {
    public static BufferedImage menuBackground;
    public static BufferedImage collectionBackground;
    public static BufferedImage shopBackground;
    public static BufferedImage playBackground;
    public static BufferedImage statusBackground;
    public static BufferedImage passiveBackground;
    public static BufferedImage[] nextPage;
    public static BufferedImage[] myCollectionIcon;
    public static BufferedImage[] shop;
    public static BufferedImage[] play;
    public static BufferedImage[] deckReader;
    public static BufferedImage[] status;
    public static BufferedImage[] Quit;
    public static BufferedImage[] neutral;
    public static BufferedImage[] Mage;
    public static BufferedImage[] Warlock;
    public static BufferedImage[] Hunter;
    public static BufferedImage[] Rogue;
    public static BufferedImage[] Paladin;
    public static BufferedImage[][] heroes;
    public static BufferedImage[] lock;
    public static BufferedImage[] unlock;
    public static BufferedImage[] lockButton;
    public static BufferedImage[] newDeck;
    public static BufferedImage[] delete;
    public static BufferedImage[] changeHero;
    public static BufferedImage[][] mana;
    public static BufferedImage[] back;
    public static BufferedImage[] done;
    public static BufferedImage[] start;
    public static BufferedImage[] menu;
    public static BufferedImage[] exit;
    public static BufferedImage[] endTurn;
    public static BufferedImage[] playInCollection;
    public static BufferedImage[] search;
    public static BufferedImage[] searchIcon;
    public static BufferedImage[] writeName;
    public static BufferedImage[] land;
    public static BufferedImage[] doneWriteHeroName;
    public static BufferedImage[][] heroName;
    public static BufferedImage[][] heroPicture;
    public static BufferedImage[][] heroesPlay;
    public static BufferedImage[][] heroesHeroPowerPlay;
    public static BufferedImage[][] passive;

    public static void init() {
        menuBackground = ImageLoader.loadImage("/texture/menu background.png");
        collectionBackground = ImageLoader.loadImage("/texture/collection background.png");
        shopBackground = ImageLoader.loadImage("/texture/shop background.png");
        playBackground = ImageLoader.loadImage("/texture/play background.png");
        statusBackground = ImageLoader.loadImage("/texture/status background.png");
        passiveBackground = ImageLoader.loadImage("/texture/passive background.png");
        myCollectionIcon = new BufferedImage[]{ImageLoader.loadImage("/texture/mycollection1.png")
                , ImageLoader.loadImage("/texture/mycollection2.png")};
        shop = new BufferedImage[]{ImageLoader.loadImage("/texture/shop1.png")
                , ImageLoader.loadImage("/texture/shop2.png")};
        writeName = new BufferedImage[]{ImageLoader.loadImage("/texture/writeName1.png")
                , ImageLoader.loadImage("/texture/writeName2.png")};
        doneWriteHeroName = new BufferedImage[]{ImageLoader.loadImage("/texture/doneWriteHeroName1.png")
                , ImageLoader.loadImage("/texture/doneWriteHeroName2.png")};
        play = new BufferedImage[]{ImageLoader.loadImage("/texture/play1.png")
                , ImageLoader.loadImage("/texture/play2.png")};
        deckReader = new BufferedImage[]{ImageLoader.loadImage("/texture/deckReader1.png")
                , ImageLoader.loadImage("/texture/deckReader2.png")};
        status = new BufferedImage[]{ImageLoader.loadImage("/texture/status1.png")
                , ImageLoader.loadImage("/texture/status2.png")};
        Quit = new BufferedImage[]{ImageLoader.loadImage("/texture/Quit1.png")
                , ImageLoader.loadImage("/texture/Quit2.png")};
        neutral = new BufferedImage[]{ImageLoader.loadImage("/texture/neutral1.png")
                , ImageLoader.loadImage("/texture/neutral2.png")};
        Mage = new BufferedImage[]{ImageLoader.loadImage("/texture/Mage1.png")
                , ImageLoader.loadImage("/texture/Mage2.png")};
        Rogue = new BufferedImage[]{ImageLoader.loadImage("/texture/Rogue1.png")
                , ImageLoader.loadImage("/texture/Rogue2.png")};
        Paladin = new BufferedImage[]{ImageLoader.loadImage("/texture/Paladin1.png")
                , ImageLoader.loadImage("/texture/Paladin2.png")};
        Warlock = new BufferedImage[]{ImageLoader.loadImage("/texture/Warlock1.png")
                , ImageLoader.loadImage("/texture/Warlock2.png")};
        Hunter = new BufferedImage[]{ImageLoader.loadImage("/texture/Hunter1.png")
                , ImageLoader.loadImage("/texture/Hunter2.png")};
        start = new BufferedImage[]{ImageLoader.loadImage("/texture/start1.png")
                , ImageLoader.loadImage("/texture/start2.png")};
        heroes = new BufferedImage[][]{Mage, Warlock, Rogue, Paladin, Hunter};
        nextPage = new BufferedImage[]{ImageLoader.loadImage("/texture/next page1.png")
                , ImageLoader.loadImage("/texture/next page2.png")};
        changeHero = new BufferedImage[]{ImageLoader.loadImage("/texture/change hero1.png")
                , ImageLoader.loadImage("/texture/change hero2.png")};
        delete = new BufferedImage[]{ImageLoader.loadImage("/texture/delete1.png")
                , ImageLoader.loadImage("/texture/delete2.png")};
        lock = new BufferedImage[]{ImageLoader.loadImage("/texture/lock1.png")
                , ImageLoader.loadImage("/texture/lock2.png")};
        lockButton = new BufferedImage[]{ImageLoader.loadImage("/texture/lockButton1.png")
                , ImageLoader.loadImage("/texture/lockButton2.png")};
        unlock = new BufferedImage[]{ImageLoader.loadImage("/texture/unlock1.png")
                , ImageLoader.loadImage("/texture/unlock2.png")};
        newDeck = new BufferedImage[]{ImageLoader.loadImage("/texture/new deck1.png")
                , ImageLoader.loadImage("/texture/new deck2.png")};
        playInCollection = new BufferedImage[]{ImageLoader.loadImage("/texture/play in collection1.png")
                , ImageLoader.loadImage("/texture/play in collection2.png")};
        exit = new BufferedImage[]{ImageLoader.loadImage("/texture/exit1.png")
                , ImageLoader.loadImage("/texture/exit2.png")};
        endTurn = new BufferedImage[]{ImageLoader.loadImage("/texture/end turn1.png")
                , ImageLoader.loadImage("/texture/end turn2.png")};
        menu = new BufferedImage[]{ImageLoader.loadImage("/texture/menu1.png")
                , ImageLoader.loadImage("/texture/menu2.png")};
        back = new BufferedImage[]{ImageLoader.loadImage("/texture/back1.png")
                , ImageLoader.loadImage("/texture/back2.png")};
        done = new BufferedImage[]{ImageLoader.loadImage("/texture/done1.png")
                , ImageLoader.loadImage("/texture/done2.png")};
        land = new BufferedImage[]{ImageLoader.loadImage("/texture/land1.png")
                , ImageLoader.loadImage("/texture/land2.png")};
        search = new BufferedImage[]{ImageLoader.loadImage("/texture/search1.png")
                , ImageLoader.loadImage("/texture/search2.png")};
        searchIcon = new BufferedImage[]{ImageLoader.loadImage("/texture/search icon1.png")
                , ImageLoader.loadImage("/texture/search icon2.png")};
        mana = new BufferedImage[11][2];
        for (int i = 0; i < 11; i++) {
            String s1 = "mana" + (i) + "" + 1;
            String s2 = "mana" + (i) + "" + 2;
            mana[i] = new BufferedImage[]{ImageLoader.loadImage("/texture/" + s1 + ".png")
                    , ImageLoader.loadImage("/texture/" + s2 + ".png")};
        }
        heroName = new BufferedImage[5][2];
        for (int i = 0 ; i < 5 ; i++){
            String s1 = "hero" + (i+1) + "" + 1;
            String s2 = "hero" + (i+1) + "" + 2;
            heroName[i] = new BufferedImage[]{ImageLoader.loadImage("/texture/hero name/" + s1 + ".png")
                    , ImageLoader.loadImage("/texture/hero name/" + s2 + ".png")};
        }
        heroPicture = new BufferedImage[5][2];
        for (int i = 0 ; i < 5 ; i++){
            String s1 = "hero" + (i+1) + "" + 1;
            String s2 = "hero" + (i+1) + "" + 2;
            heroPicture[i] = new BufferedImage[]{ImageLoader.loadImage("/texture/hero picture/" + s1 + ".png")
                    , ImageLoader.loadImage("/texture/hero picture/" + s2 + ".png")};
        }
        heroesPlay = new BufferedImage[5][2];
        for (int i = 0 ; i < 5 ; i++){
            String s1 = "hero" + (i+1) + "" + 1;
            String s2 = "hero" + (i+1) + "" + 4;
            heroesPlay[i] = new BufferedImage[]{ImageLoader.loadImage("/texture/heroes in play/" + s1 + ".png")
                    , ImageLoader.loadImage("/texture/heroes in play/" + s2 + ".png")};
        }
        heroesHeroPowerPlay = new BufferedImage[5][2];
        for (int i = 0 ; i < 5 ; i++){
            String s1 = "hero" + (i+1) + "" + 2;
            String s2 = "hero" + (i+1) + "" + 3;
            heroesHeroPowerPlay[i] = new BufferedImage[]{ImageLoader.loadImage("/texture/heroes in play/" + s1 + ".png")
                    , ImageLoader.loadImage("/texture/heroes in play/" + s2 + ".png")};
        }
        passive = new BufferedImage[5][2];
        for (int i = 0 ; i < 5 ; i++){
            String s1 = "passive" + (i+1) + "" + 1;
            String s2 = "passive" + (i+1) + "" + 2;
            passive[i] = new BufferedImage[]{ImageLoader.loadImage("/texture/passive/" + s1 + ".png")
                    , ImageLoader.loadImage("/texture/passive/" + s2 + ".png")};
        }
    }
}
