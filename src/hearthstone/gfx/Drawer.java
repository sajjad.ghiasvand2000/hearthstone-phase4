package hearthstone.gfx;

import hearthstone.configs.CollectionStateConfigs;
import hearthstone.constants.Constants;

import java.awt.*;
import java.util.Collection;

public class Drawer {
    private static CollectionStateConfigs c = CollectionStateConfigs.getInstance();

    public static void rightBorderCollection(Graphics2D g2D) {
        g2D.setColor(Color.BLACK);
        g2D.fillRect(c.get("initialXPosDeckButton"), c.get("initialYPosDeckButton"),
                c.get("widthDeckButton") + 2 * c.get("horizontalDistanceDeckButton"),
                (c.get("deckNumber") + 3) * c.get("verticalDistanceDeckButton") +
                        (c.get("deckNumber") + 2) * c.get("heightDeckButton"));
    }

    public static void Massage(String prompt, Graphics2D g2D, String name) {
        Font font = new Font("Helvetica", Font.BOLD, 60);
        FontMetrics fontMetrics = g2D.getFontMetrics(font);
        int width = fontMetrics.stringWidth(prompt);
        g2D.setColor(Color.RED);
        g2D.setFont(font);
        int y;
        if (name.equals("player1")) y = 60;
        else if (name.equals("player2")) y = -60;
        else y = 0;
        g2D.drawString(prompt, (Constants.COMPUTER_WIDTH - width) / 2, (Constants.COMPUTER_HEIGHT - 60) / 2 + y);
    }

    public static void Wallet(String prompt, Graphics2D g2D) {
        Font font = new Font("Helvetica", Font.BOLD, 30);
        FontMetrics fontMetrics = g2D.getFontMetrics(font);
        int width = fontMetrics.stringWidth(prompt);
        g2D.setColor(Color.RED);
        g2D.setFont(font);
        g2D.drawString(prompt, Constants.COMPUTER_WIDTH - width - 50, Constants.COMPUTER_HEIGHT - 30 - 50);
    }

}
