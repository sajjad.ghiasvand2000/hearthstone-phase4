package hearthstone.configs;

public class MenuStateConfigs {
    private String name;
    private Configs properties;
    private static MenuStateConfigs menuStateConfigs;

    private MenuStateConfigs(String name){
        properties = ConfigLoader.getInstance().getMenuStateConfigs(name);
    }

    public static MenuStateConfigs getInstance(String name){
        if (menuStateConfigs == null)
            menuStateConfigs = new MenuStateConfigs(name);
        return menuStateConfigs;
    }

    public static MenuStateConfigs getInstance(){
        return getInstance("MENU_STATE_CONFIG_FILE");
    }

    public int getMenuSateConfigs(String name){
        return properties.readInteger(name);
    }
}
