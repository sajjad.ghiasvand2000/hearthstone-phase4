package hearthstone.configs;

public class PassiveStateConfigs {
    private String name;
    private Configs properties;
    private static PassiveStateConfigs passiveStateConfigs;

    private PassiveStateConfigs(String name){
        properties = ConfigLoader.getInstance().getPassiveStateConfigs(name);
    }

    public static PassiveStateConfigs getInstance(String name){
        if (passiveStateConfigs == null)
            passiveStateConfigs = new PassiveStateConfigs(name);
        return passiveStateConfigs;
    }

    public static PassiveStateConfigs getInstance(){
        return getInstance("PASSIVE_STATE_CONFIG_FILE");
    }

    public int get(String name){
        return properties.readInteger(name);
    }
}
