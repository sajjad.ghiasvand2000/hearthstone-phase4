package hearthstone.utils;

import server.Entity.cards.Card;
import server.Entity.MainPlayer;
import server.Entity.hero.Hero;
import server.Entity.hero.HeroClass;
import server.data.Log;
import server.data.MyFile;
import hearthstone.configs.MenuStateConfigs;
import hearthstone.gfx.Asserts;
import hearthstone.ui.UIManager;
import hearthstone.ui.UIRecImage;

public class Util {
    private static MenuStateConfigs m = MenuStateConfigs.getInstance();

    public static int searchMainPlayerCards(Card card) {
        for (int i = 0; i < MainPlayer.getInstance().getEntireCards().size(); i++) {
            Card entireCard = MainPlayer.getInstance().getEntireCards().get(i);
            if (entireCard.equals(card))
                return i;
        }
        return -1;
    }

    public static void exit(UIManager uiManager) {
        uiManager.addButton(new UIRecImage(m.getMenuSateConfigs("initialXPosMenu") + m.getMenuSateConfigs("widthMenu") +
                m.getMenuSateConfigs("horizontalDistance"), m.getMenuSateConfigs("initialYPosMenu"), m.getMenuSateConfigs("widthMenu"),
                m.getMenuSateConfigs("heightMenu"), Asserts.exit, () -> {
            Log.body("exit", "exit");
            MyFile.saveChanges();
            System.exit(0);
        }));
    }

    public static Hero finedHero(HeroClass heroClass){
        for (Hero hero : MainPlayer.getInstance().getHeroes()) {
            if (hero.getHeroClass().toString().equals(heroClass.toString())){
                return hero;
            }
        }
        return null;
    }

    public static Card findCardFromMainPlayer(String card){
        for (Card entireCard : MainPlayer.getInstance().getEntireCards()) {
            if (entireCard.getName().equals(card))
                return entireCard;
        }
        return null;
    }
}
