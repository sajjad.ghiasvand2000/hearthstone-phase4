package hearthstone;

import hearthstone.display.Display;
import hearthstone.input.KeyManager;
import hearthstone.input.MouseManager;
import hearthstone.states.changeCardState.ChangeCardState;
import hearthstone.states.infoPassive.InfoPassive;
import hearthstone.states.menuState.MenuState;
import hearthstone.states.collectionState.CollectionState;
import hearthstone.states.playState.graphic.PlayState;
import hearthstone.states.shopState.ShopState;
import hearthstone.states.statusState.graphic.StatusState;

public class Handler {
    private Game game;

    public Handler(Game game) {
        this.game = game;
    }

    public Display getDisplay() {
        return game.getDisplay();
    }

    public CollectionState getCollectionState() {
        return game.getCollectionState();
    }

    public ShopState getShopState() {
        return game.getShopState();
    }

    public MenuState getMenuState() {
        return game.getMenuState();
    }

    public PlayState getPlayState() {
        return game.getPlayState();
    }

    public InfoPassive getInfoPassive1() {
        return game.getInfoPassive1();
    }

    public InfoPassive getInfoPassive2() {
        return game.getInfoPassive2();
    }

    public StatusState getStatusState() {
        return game.getStatusState();
    }

    public void setPlayState(PlayState playState) {
        game.setPlayState(playState);
    }

    public void setInfoPassive1(InfoPassive infoPassive) {
        game.setInfoPassive1(infoPassive);
    }

    public void setInfoPassive2(InfoPassive infoPassive) {
        game.setInfoPassive2(infoPassive);
    }

    public void setStatusState(StatusState statusState) {
        game.setStatusState(statusState);
    }

    public MouseManager getMouseManager() {
        return game.getMouseManager();
    }

    public KeyManager getKeyManager() {
        return game.getKeyManager();
    }

    public void setChangeCardState1(ChangeCardState changeCardState1) {
        game.setChangeCardState1(changeCardState1);
    }

    public void setChangeCardState2(ChangeCardState changeCardState2) {
        game.setChangeCardState1(changeCardState2);
    }

    public ChangeCardState getChangeCardState1(ChangeCardState changeCardState1){
       return game.getChangeCardState1();
    }

    public ChangeCardState getChangeCardState2(ChangeCardState changeCardState2){
        return game.getChangeCardState2();
    }

}
