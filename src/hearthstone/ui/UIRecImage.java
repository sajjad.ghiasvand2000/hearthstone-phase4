package hearthstone.ui;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class UIRecImage extends UIObject {
    public UIRecImage(float x, float y, int width, int height, BufferedImage[] images, ClickListener clicker) {
        super(x, y, width, height, images, clicker);
    }

//    @Override
//    public void tick() {}

    @Override
    public void render(Graphics2D g2D) {
        if (hovering)
            g2D.drawImage(images[1], (int)x, (int)y, width, height, null);
        else
            g2D.drawImage(images[0], (int)x, (int)y, width, height, null);
    }

    @Override
    public void onClick() {
        clicker.onClick();
    }

}
