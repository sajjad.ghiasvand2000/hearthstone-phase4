package hearthstone.ui;

import server.Entity.cards.Card;

import java.awt.*;
import java.awt.image.BufferedImage;

public class UICardImage extends UIObject {
    protected Card card;

    public UICardImage(Card card, float x, float y, int width, int height, BufferedImage[] bufferedImages, ClickListener clicker) {
        super(x, y, width, height, bufferedImages, clicker);
        this.card = card;
    }

//    @Override
//    public void tick() {}

    @Override
    public void render(Graphics2D g2D) {
        if (hovering)
            g2D.drawImage(images[1], (int)x, (int)y, width, height, null);
        else
            g2D.drawImage(images[0], (int)x, (int)y, width, height, null);
    }

    @Override
    public void onClick() {
        clicker.onClick();
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
