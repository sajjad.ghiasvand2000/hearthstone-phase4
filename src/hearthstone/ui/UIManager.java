package hearthstone.ui;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import hearthstone.Handler;

public class UIManager {
    private Handler handler;
    private List<UIObject> buttons;


    public UIManager(Handler handler) {
        this.handler = handler;
        buttons = new ArrayList<>();
    }

    public void tick() {
        onMouseMove();
        onMouseRelease();
        for (UIObject button : buttons) {
            button.tick();
        }
    }

    public void render(Graphics2D g2D) {
        for (UIObject button : buttons) {
            button.render(g2D);
        }
    }

    public void onMouseMove() {
        for (UIObject button : buttons) {
            button.onMouseMove();
        }

    }

    public void onMouseRelease() {
        for (UIObject button : buttons) {
            button.onMouseRelease();
        }

    }

    public void addButton(UIObject o) {
        buttons.add(o);
    }

    public List<UIObject> getButtons() {
        return buttons;
    }

    public void setButtons(List<UIObject> objects) {
        this.buttons = objects;
    }

    public void removeAll() {
        buttons.clear();
    }
}
