package server.Entity;

import server.Entity.cards.Card;
import server.Entity.hero.Hero;
import console.AddRemove;

import java.util.ArrayList;
import java.util.List;

public class Player extends AddRemove {

    private String username;
    private String password;
    private int userId;
    private int diamond ;
    private ArrayList<Card> entireCards;
    private Deck[] decks;
    private List<Hero> heroes;

    public Player() {
        decks = new Deck[13];
    }

    public void setHeroes(List<Hero> heroes) {
        this.heroes = heroes;
    }

    public List<Hero> getHeroes() {
        return heroes;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getDiamond() {
        return diamond;
    }

    public void setUsername(String name) {
        this.username = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setDiamond(int diamond) {
        this.diamond = diamond;
    }

    public ArrayList<Card> getEntireCards() {
        return entireCards;
    }

    public Deck[] getDecks() {
        return decks;
    }

    public void setEntireCards(ArrayList<Card> entireCards) {
        this.entireCards = entireCards;
    }

    public void setDecks(Deck[] decks) {
        this.decks = decks;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setDeck(Deck deck, int indexDeck){
        decks[indexDeck] = deck;
    }

}
