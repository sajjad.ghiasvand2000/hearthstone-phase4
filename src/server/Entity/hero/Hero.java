package server.Entity.hero;

import server.Entity.cards.Card;
import console.AddRemove;
import hearthstone.states.playState.logic.GamePlayerLogic;

import java.util.ArrayList;
import java.util.List;

public abstract class Hero{

    private HeroClass heroClass;
    protected int HP;
    protected int MAX_HP;
    private List<Card> cards;
    private String[] texturePath;
    private String[] heroTexture;

    public Hero(HeroClass heroClass, int HP) {
        this.heroClass = heroClass;
        this.HP = HP;
    }

    public Hero(Hero hero){
        this.heroClass = hero.heroClass;
        this.cards = new ArrayList<>(hero.cards);
        this.texturePath = hero.texturePath;
    }

    public Hero() {}

    public abstract void heroPower(GamePlayerLogic GPL);

    public Hero(HeroClass heroClass) {
        this.heroClass = heroClass;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cardsName) {
        this.cards = cardsName;
    }

    public HeroClass getHeroClass() {
        return heroClass;
    }

    public void setHeroClass(HeroClass heroClass) {
        this.heroClass = heroClass;
    }

    public int getHP() {
        return HP;
    }

    public String[] getHeroTexture() {
        return heroTexture;
    }

    public void setHeroTexture(String[] heroTexture) {
        this.heroTexture = heroTexture;
    }

    public void setTexturePath(String[] texturePath) {
        this.texturePath = texturePath;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }

    public int getMAX_HP() {
        return MAX_HP;
    }

    public void setMAX_HP(int MAX_HP) {
        this.MAX_HP = MAX_HP;
    }
}
