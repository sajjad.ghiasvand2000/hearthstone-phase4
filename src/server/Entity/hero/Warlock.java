package server.Entity.hero;

import server.Entity.EntityUtils;
import hearthstone.states.playState.logic.GamePlayerLogic;

import java.util.Random;

public class Warlock extends Hero {

    public Warlock(Hero hero){
        super(hero);
        HP = 35;
        MAX_HP = 35;
    }

    @Override
    public void heroPower(GamePlayerLogic GPL) {
        GPL.getDeck().getHero().setHP(GPL.getDeck().getHero().getHP() - 2);
        GPL.changeHeroUpdateFlag();
        Random random = new Random();
        int x = random.nextInt(2);
        if (x == 0){
            EntityUtils.addAMinionHPAndAttack(GPL);
        } else {
            EntityUtils.addACardFromDeckToHand(GPL);
        }
    }

}
