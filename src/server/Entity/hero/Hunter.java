package server.Entity.hero;

import hearthstone.states.playState.logic.GamePlayerLogic;

public class Hunter extends Hero {

    public Hunter(Hero hero){
        super(hero);
        HP = 30;
        MAX_HP = 30;
    }

    @Override
    public void heroPower(GamePlayerLogic GPL) {
        System.out.println("hunter");
    }

}