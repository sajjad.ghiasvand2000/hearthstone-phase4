package server.Entity.hero;

public enum HeroClass {
    Mage,
    Rogue,
    Warlock,
    NATURAL,
    Hunter,
    Paladin
}
