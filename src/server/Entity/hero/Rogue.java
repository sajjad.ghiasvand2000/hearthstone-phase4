package server.Entity.hero;

import server.Entity.EntityUtils;
import server.Entity.cards.Card;
import server.data.Log;
import hearthstone.states.playState.MainMapper;
import hearthstone.states.playState.logic.GamePlayerLogic;

public class Rogue extends Hero {

    public Rogue(Hero hero){
        super(hero);
        HP = 30;
        MAX_HP = 30;
    }

    @Override
    public void heroPower(GamePlayerLogic GPL) {
        Card card;
        GamePlayerLogic GPL1 = MainMapper.getGPL1();
        GamePlayerLogic GPL2 = MainMapper.getGPL2();
        if (GPL.getName().equals("player1")){
            card = EntityUtils.getACardFromDeck(GPL2);
        }else {
            card = EntityUtils.getACardFromDeck(GPL1);
        }
        if (card != null && GPL.getContainDeckCard().size() < 12 && GPL.getMana() > 2){
            Log.body("a card was stolen from enemy deck", card.getName());
            GPL.setMana(GPL.getMana() - 3 + GPL.getFreePowerPassive());
            GPL.getContainDeckCard().add(card);
            GPL.setExtraCard(card);
            if (GPL.getName().equals("player1"))
                GPL2.setNumberOfCards(GPL2.getNumberOfCards() - 1);
            else GPL1.setNumberOfCards(GPL1.getNumberOfCards() - 1);
        }
    }

}
