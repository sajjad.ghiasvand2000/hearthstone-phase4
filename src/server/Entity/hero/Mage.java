package server.Entity.hero;

import hearthstone.states.playState.logic.GamePlayerLogic;

public class Mage extends Hero {

    public Mage() {
    }

    public Mage(Hero hero){
        super(hero);
        HP = 30;
        MAX_HP = 30;
    }

    @Override
    public void heroPower(GamePlayerLogic GPL) {
        GPL.getHeroLogic().setHeroPowerWantAttack(true);
    }

}
