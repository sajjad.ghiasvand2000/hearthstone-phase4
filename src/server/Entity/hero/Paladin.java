package server.Entity.hero;

import server.Entity.EntityUtils;
import server.Entity.cards.minion.SecurityRover;
import hearthstone.states.playState.logic.GamePlayerLogic;
import hearthstone.utils.Util;

public class Paladin extends Hero {

    public Paladin(Hero hero) {
        super(hero);
        HP = 30;
        MAX_HP = 30;
    }

    @Override
    public void heroPower(GamePlayerLogic GPL) {
        if (GPL.getMana() > 1) {
            new Thread(() -> {
                addMinion(GPL);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                addMinion(GPL);
            }).start();
        }
    }

    private void addMinion(GamePlayerLogic GPL) {
        int x = EntityUtils.finedZeroRandomIndex(GPL);
        if (x != -1) {
            GPL.setMana(GPL.getMana() - 2 + GPL.getFreePowerPassive());
            SecurityRover securityRover = new SecurityRover((SecurityRover) Util.findCardFromMainPlayer("Security Rover"));
            securityRover.setHP(1);
            securityRover.setAttack(1);
            GPL.setOneContainLandCard(securityRover, x);
        }

    }

}
