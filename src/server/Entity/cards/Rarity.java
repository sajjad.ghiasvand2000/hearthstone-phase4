package server.Entity.cards;

public enum Rarity{
    COMMON,
    RARE,
    EPIC,
    LEGENDARY
}
