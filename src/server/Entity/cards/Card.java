package server.Entity.cards;

import server.Entity.hero.HeroClass;

import java.util.Objects;

public class Card implements Comparable<Card> {
    protected String name;
    protected int cost;
    protected int mana;
    protected Rarity rarity;
    protected HeroClass heroClass;
    protected Type type;
    protected String description;
    protected String[] texturePath;
    protected boolean lock;
    private int numberDraw;

    public Card(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String[] texturePath, boolean lock) {
        this.name = name;
        this.cost = cost;
        this.mana = mana;
        this.rarity = rarity;
        this.heroClass = heroClass;
        this.type = type;
        this.description = description;
        this.texturePath = texturePath;
        this.lock = lock;
    }

    public Card(Card card){
        this.name = card.name;
        this.cost = card.cost;
        this.mana = card.mana;
        this.rarity = card.rarity;
        this.heroClass = card.heroClass;
        this.type = card.type;
        this.description = card.description;
        this.texturePath = card.texturePath;
        this.lock = card.lock;
    }

    public String getName() {
        return name;
    }

    public HeroClass getHeroClass() {
        return heroClass;
    }

    public int getCost() {
        return cost;
    }

    public int getMana() {
        return mana;
    }

    public Type getType() {
        return type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public void setHeroClass(HeroClass heroClass) {
        this.heroClass = heroClass;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public boolean isLock() {
        return lock;
    }

    public void setLock(boolean lock) {
        this.lock = lock;
    }

    public String[] getTexturePath() {
        return texturePath;
    }

    public int getNumberDraw() {
        return numberDraw;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return name.equals(card.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(Card card) {
        return Integer.compare(this.getMana(), card.getMana());
    }
}

