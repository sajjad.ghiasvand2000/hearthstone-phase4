package server.Entity.cards.weapon;

import server.Entity.EntityUtils;
import server.Entity.cards.Card;
import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.cards.minion.Minion;
import server.Entity.hero.HeroClass;
import com.google.gson.internal.LinkedTreeMap;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public abstract class Weapon extends Card {
    private Integer durability;
    private Integer attack;
    private String[] WeaponTexture;

    public Weapon(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer durability, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, texturePath, lock );
        WeaponTexture = new String[2];
        this.durability = durability;
        this.attack = attack;
    }

    public static Object factory(LinkedTreeMap map) {
        Class clazz = null;
        try {
            clazz = Class.forName("server.Entity.cards.weapon." + EntityUtils.buildName((String) map.get("name")));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Constructor constructor = null;
        constructor = clazz.getConstructors()[0];
        Double cost = (Double) map.get("cost");
        Double mana = (Double) map.get("mana");
        Double durability = (Double) map.get("durability");
        Double attack = (Double) map.get("attack");
        Object o = null;
        try {
            o = constructor.newInstance(map.get("name"), cost.intValue(), mana.intValue(), Rarity.valueOf((String)map.get("rarity")),
                    HeroClass.valueOf((String)map.get("heroClass")), Type.valueOf((String)map.get("type")), map.get("description"),
                    durability.intValue(), attack.intValue(), EntityUtils.convertArrayListToArray((ArrayList)map.get("texturePath")), map.get("lock"));
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return o;
    }

    public Weapon factory2() {
        Class clazz = null;
        try {
            clazz = Class.forName("server.Entity.cards.weapon." + EntityUtils.buildName(name));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Constructor constructor = clazz.getConstructors()[0];
        Object o = null;
        try {
            o = constructor.newInstance(name, cost, mana, rarity, heroClass, type, description, durability, attack, texturePath, lock);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return (Weapon) o;
    }

    public void weaponAttackMinion(Minion minion){
        minion.setHP(minion.getHP() - attack);
        durability = durability - 1;
    }

    public Integer getDurability() {
        return durability;
    }

    public void setDurability(Integer durability) {
        this.durability = durability;
    }

    public String[] getWeaponTexture() {
        return WeaponTexture;
    }

    public void setWeaponTexture(String[] weaponTexture) {
        WeaponTexture = weaponTexture;
    }

    public Integer getAttack() {
        return attack;
    }

    public void setAttack(Integer attack) {
        this.attack = attack;
    }
}
