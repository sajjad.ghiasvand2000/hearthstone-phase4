package server.Entity.cards.weapon;

import server.Entity.EntityUtils;
import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.hero.HeroClass;
import hearthstone.states.playState.logic.GamePlayerLogic;

public class WagglePick extends Deathrattle {
    public WagglePick(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer durability, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, durability, attack, texturePath, lock);
    }

    @Override
    public void deathrattle(GamePlayerLogic GPL) {
        EntityUtils.addACardFromDeckToHand(GPL);
    }
}
