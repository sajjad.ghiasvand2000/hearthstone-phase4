package server.Entity.cards.weapon;

import server.Entity.cards.Card;
import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.cards.minion.Minion;
import server.Entity.hero.HeroClass;
import hearthstone.states.playState.logic.GamePlayerLogic;

public class SerratedTooth extends Deathrattle {
    public SerratedTooth(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer durability, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, durability, attack, texturePath, lock);
    }

    @Override
    public void deathrattle(GamePlayerLogic GPL) {
        for (Minion minion : GPL.getContainLandCard()) {
            if (minion != null) {
                minion.setRush(true);
            }
        }
        for (Card card : GPL.getContainDeckCard()) {
            if (card.getType().equals(Type.MINION)) {
                ((Minion) card).setRush(true);
            }
        }
    }
}
