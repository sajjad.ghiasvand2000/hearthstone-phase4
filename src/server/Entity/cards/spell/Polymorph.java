package server.Entity.cards.spell;

import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.cards.minion.Minion;
import server.Entity.cards.minion.SecurityRover;
import server.Entity.hero.HeroClass;
import hearthstone.states.playState.logic.GamePlayerLogic;
import hearthstone.utils.Util;

public class Polymorph extends ComplexSpell {
    public Polymorph(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    @Override
    public void complexSpell(GamePlayerLogic GPL, Minion minion) {
        SecurityRover securityRover = new SecurityRover((SecurityRover) Util.findCardFromMainPlayer("Security Rover"));
        securityRover.setHP(1);
        securityRover.setAttack(1);
        GPL.setOneContainLandCard(securityRover, GPL.getMinionLogic().getIndexMinionWantAttack());
    }
}
