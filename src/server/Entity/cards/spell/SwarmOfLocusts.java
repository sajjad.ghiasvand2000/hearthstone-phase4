package server.Entity.cards.spell;

import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.cards.minion.SecurityRover;
import server.Entity.hero.HeroClass;
import hearthstone.states.playState.logic.GamePlayerLogic;
import hearthstone.utils.Util;

public class SwarmOfLocusts extends SingleSpell {
    public SwarmOfLocusts(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    @Override
    public void singleSpell(GamePlayerLogic GPL) {
        for (int i = 0 ; i < 7 ; i++) {
            int finalI = i;
            new Thread(() -> {
                locusts(GPL, finalI);
            }).start();
        }
        GPL.changeSpecialUpdateFlag();
    }

    private void locusts(GamePlayerLogic GPL, int i) {
        if (GPL.getContainLandCard()[i] == null) {
            SecurityRover securityRover = new SecurityRover((SecurityRover) Util.findCardFromMainPlayer("Security Rover"));
            securityRover.setHP(1);
            securityRover.setAttack(1);
            securityRover.setCharge(true);
            GPL.setContainLandCard(securityRover, i);
        }
    }

}
