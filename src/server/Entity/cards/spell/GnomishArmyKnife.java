package server.Entity.cards.spell;

import server.Entity.EntityUtils;
import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.cards.minion.Minion;
import server.Entity.hero.HeroClass;
import hearthstone.states.playState.logic.GamePlayerLogic;

public class GnomishArmyKnife extends SingleSpell {
    public GnomishArmyKnife(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    @Override
    public void singleSpell(GamePlayerLogic GPL) {
        int x = EntityUtils.finedNonZeroRandomIndex(GPL);
        if (x != -1) {
            Minion minion = GPL.getContainLandCard()[x];
            minion.setCharge(false);
            minion.setWindFury(true);
            minion.setShield(true);
            minion.setTaunt(true);
            minion.setPoisonous(true);
            minion.setLifeSteal(true);
        }
    }

}
