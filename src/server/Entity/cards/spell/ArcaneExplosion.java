package server.Entity.cards.spell;

import server.Entity.EntityUtils;
import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.hero.HeroClass;
import hearthstone.states.playState.MainMapper;
import hearthstone.states.playState.logic.GamePlayerLogic;

public class ArcaneExplosion extends SingleSpell {
    public ArcaneExplosion(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    @Override
    public void singleSpell(GamePlayerLogic GPL) {
        if (GPL.getName().equals("player1")) {
            EntityUtils.decreaseHPFromAllMinion(MainMapper.getGPL2(), 1);
        } else {
            EntityUtils.decreaseHPFromAllMinion(MainMapper.getGPL1(), 1);
        }
    }
}
