package server.Entity.cards.spell;

import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.hero.HeroClass;
import hearthstone.states.playState.logic.GamePlayerLogic;

public abstract class QuestReward extends Spell {
    public QuestReward(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    public abstract void start(GamePlayerLogic GPL);
    public abstract boolean quest(GamePlayerLogic GPL);
    public abstract void reward(GamePlayerLogic GPL);
    public abstract double developmentPercent(GamePlayerLogic GPL);
}
