package server.Entity.cards.spell;

import server.Entity.EntityUtils;
import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.cards.minion.RotnestDrake;
import server.Entity.hero.HeroClass;
import hearthstone.states.playState.logic.GamePlayerLogic;
import hearthstone.utils.Util;

public class LearnDraconic extends QuestReward {
    public LearnDraconic(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    @Override
    public void start(GamePlayerLogic GPL) {
       GPL.getSpellLogic().setSpellQuest(0);
    }

    @Override
    public boolean quest(GamePlayerLogic GPL) {
        return GPL.getSpellLogic().getSpellQuest() >= 8;
    }

    @Override
    public void reward(GamePlayerLogic GPL) {
        int x = EntityUtils.finedZeroRandomIndex(GPL);
        if (x != -1){
            RotnestDrake rotnestDrake = new RotnestDrake((RotnestDrake) Util.findCardFromMainPlayer("Rotnest Drake"));
            rotnestDrake.setHP(6);
            GPL.setOneContainLandCard(rotnestDrake, x);
        }
    }

    @Override
    public double developmentPercent(GamePlayerLogic GPL) {
        return GPL.getSpellLogic().getSpellQuest()/8.0;
    }
}
