package server.Entity.cards.spell;

import server.Entity.EntityUtils;
import server.Entity.cards.Card;
import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.hero.HeroClass;
import com.google.gson.internal.LinkedTreeMap;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public abstract class Spell extends Card {
    protected String QuestAndReward;

    public Spell(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, texturePath, lock);
        QuestAndReward = questAndReward;
    }

    public static Object factory(LinkedTreeMap map) {
        Class clazz = null;
        try {
            clazz = Class.forName("server.Entity.cards.spell." + EntityUtils.buildName((String) map.get("name")));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Constructor constructor = null;
        constructor = clazz.getConstructors()[0];
        Double cost = (Double) map.get("cost");
        Double mana = (Double) map.get("mana");
        Object o = null;
        try {
            o = constructor.newInstance(map.get("name"), cost.intValue(), mana.intValue(), Rarity.valueOf((String)map.get("rarity")),
                    HeroClass.valueOf((String)map.get("heroClass")), Type.valueOf((String)map.get("type")), map.get("description"),
                    map.get("questAndReward"), EntityUtils.convertArrayListToArray((ArrayList)map.get("texturePath")), map.get("lock"));
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return o;
    }

    public String isQuestAndReward() {
        return QuestAndReward;
    }

    public void setQuestAndReward(String questAndReward) {
        QuestAndReward = questAndReward;
    }
}
