package server.Entity.cards.spell;

import server.Entity.MainPlayer;
import server.Entity.cards.Card;
import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.cards.weapon.Weapon;
import server.Entity.hero.HeroClass;
import hearthstone.states.playState.logic.GamePlayerLogic;

import java.util.ArrayList;
import java.util.Collections;

public class FriendlySmith extends SingleSpell {
    public FriendlySmith(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    @Override
    public void singleSpell(GamePlayerLogic GPL) {
        Weapon weapon = (Weapon)detectWeapon();
        weapon.setDurability(weapon.getDurability() + 2);
        weapon.setAttack(weapon.getAttack() + 2);
        GPL.addACardToDeck(weapon);
    }

    private Card detectWeapon(){
        ArrayList<Card> cards = new ArrayList<>();
        for (Card entireCard : MainPlayer.getInstance().getEntireCards()) {
            if (entireCard.getType().equals(Type.WEAPON)){
                cards.add(entireCard);
            }
        }
        Collections.shuffle(cards);
        return cards.get(0);
    }
}
