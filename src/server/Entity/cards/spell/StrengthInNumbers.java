package server.Entity.cards.spell;

import server.Entity.EntityUtils;
import server.Entity.cards.Card;
import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.hero.HeroClass;
import hearthstone.states.playState.logic.GamePlayerLogic;
import hearthstone.utils.Util;

public class StrengthInNumbers extends QuestReward {
    public StrengthInNumbers(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    @Override
    public void start(GamePlayerLogic GPL) {
        GPL.getMinionLogic().setMinionQuest(0);
    }

    @Override
    public boolean quest(GamePlayerLogic GPL) {
        return GPL.getMinionLogic().getMinionQuest() >= 10;
    }

    @Override
    public void reward(GamePlayerLogic GPL) {
        int x = EntityUtils.finedZeroRandomIndex(GPL);
        Card card;
        if (EntityUtils.getReward().equals(""))
            card = EntityUtils.getAMinionFromDeck(GPL);
        else card = Util.findCardFromMainPlayer(EntityUtils.getReward());
        if (x != -1 && card != null) {
            GPL.setOneContainLandCard(card, x);
            GPL.setNumberOfCards(GPL.getNumberOfCards() - 1);
        }
    }

    @Override
    public double developmentPercent(GamePlayerLogic GPL) {
        return GPL.getMinionLogic().getMinionQuest()/10.0;
    }

}
