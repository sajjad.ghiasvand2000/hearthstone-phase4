package server.Entity.cards.spell;

import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.hero.HeroClass;
import hearthstone.states.playState.logic.GamePlayerLogic;

public class BookOfSpecters extends SingleSpell {
    public BookOfSpecters(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    @Override
    public void singleSpell(GamePlayerLogic GPL) {
        int deckIndex = GPL.getDeckIndex();
        if (GPL.getDeckIndex() < GPL.getDeck().getCards().size())
            if (!GPL.getDeck().getCards().get(deckIndex).getType().equals(Type.SPELL)) {
                help(GPL, deckIndex);
                deckIndex++;
            }
        if (GPL.getDeckIndex() < GPL.getDeck().getCards().size())
            if (!GPL.getDeck().getCards().get(deckIndex).getType().equals(Type.SPELL)) {
                help(GPL, deckIndex);
                deckIndex++;
            }
        if (GPL.getDeckIndex() < GPL.getDeck().getCards().size())
            if (!GPL.getDeck().getCards().get(deckIndex).getType().equals(Type.SPELL)) {
                help(GPL, deckIndex);
            }
    }

    private void help(GamePlayerLogic GPL, int deckIndex) {
        if (GPL.getContainDeckCard().size() < 12) {
            GPL.getContainDeckCard().add(GPL.getDeck().getCards().get(deckIndex));
            GPL.setDeckIndex(GPL.getDeckIndex() + 1);
            GPL.setNumberOfCards(GPL.getNumberOfCards() - 1);
            GPL.changeUpdateFlagDeck();
        }
    }
}
