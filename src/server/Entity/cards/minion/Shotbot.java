package server.Entity.cards.minion;

import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.hero.HeroClass;

public class Shotbot extends Minion {

    private boolean reborn;

    public Shotbot(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, HP, attack, texturePath, lock);
    }

    public void reborn(){
        if (HP < 1 && !reborn){
            HP = 1;
        }
        reborn = true;
    }

}
