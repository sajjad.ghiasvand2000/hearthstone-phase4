package server.Entity.cards.minion;

import server.Entity.EntityUtils;
import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.hero.HeroClass;
import hearthstone.states.playState.logic.GamePlayerLogic;

public class Skyvateer extends Battlecry {
    public Skyvateer(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, HP, attack, texturePath, lock);
    }

    @Override
    public void battlecry(GamePlayerLogic GPL) {
        EntityUtils.addACardFromDeckToHand(GPL);
    }
}
