package server.Entity.cards.minion;

import server.Entity.EntityUtils;
import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.hero.HeroClass;
import hearthstone.states.playState.MainMapper;
import hearthstone.states.playState.logic.GamePlayerLogic;

public class RotnestDrake extends Battlecry {
    public RotnestDrake(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, HP, attack, texturePath, lock);
    }

    public RotnestDrake(RotnestDrake rotnestDrake){
        super(rotnestDrake);
    }

    @Override
    public void battlecry(GamePlayerLogic GPL) {
        int x;
        GamePlayerLogic GPL1 = MainMapper.getGPL1();
        GamePlayerLogic GPL2 = MainMapper.getGPL2();
        if (GPL.getName().equals("player1")) {
            x = EntityUtils.finedNonZeroRandomIndex(GPL2);
            if (x != -1) {
                GPL2.makeALandCardNull(x);
                GPL2.setUpdateFlagACardInLand(x);
            }
        } else {
            x = EntityUtils.finedNonZeroRandomIndex(GPL1);
            if (x != -1) {
                GPL1.makeALandCardNull(x);
                GPL1.setUpdateFlagACardInLand(x);
            }
        }
    }

}
