package server.Entity.cards.minion;

import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.hero.HeroClass;
import hearthstone.states.playState.logic.GamePlayerLogic;

public abstract class Battlecry extends Minion {
    public Battlecry(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, HP, attack, texturePath, lock);
    }

    public Battlecry(Battlecry battlecry){
        super(battlecry);
    }
    public abstract void battlecry(GamePlayerLogic GPL);
}
