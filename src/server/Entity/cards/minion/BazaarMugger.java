package server.Entity.cards.minion;

import server.Entity.MainPlayer;
import server.Entity.cards.Card;
import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.hero.HeroClass;
import hearthstone.states.playState.logic.GamePlayerLogic;
import hearthstone.utils.Util;

public class BazaarMugger extends Battlecry {
    public BazaarMugger(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, HP, attack, texturePath, lock);
    }

    @Override
    public void battlecry(GamePlayerLogic GPL) {
        GPL.addACardToDeck(Util.findCardFromMainPlayer("Archmage Arugal"));
    }

    public Card detectMinionFromAnotherClass(GamePlayerLogic GPL) {
        for (Card entireCard : MainPlayer.getInstance().getEntireCards()) {
            if (!entireCard.getHeroClass().equals(HeroClass.NATURAL) &&
                    !entireCard.getHeroClass().equals(GPL.getDeck().getHero().getHeroClass()) &&
                    entireCard.getType().equals(Type.MINION)) {
                return entireCard;
            }
        }
        return MainPlayer.getInstance().getEntireCards().get(0);
    }

}
