package server.Entity.cards.minion;

import server.Entity.EntityUtils;
import server.Entity.cards.Card;
import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.hero.HeroClass;
import com.google.gson.internal.LinkedTreeMap;
import hearthstone.states.playState.logic.GamePlayerLogic;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public abstract class Minion extends Card {
    protected Integer HP;
    private Integer attack;
    protected boolean charge;
    private boolean rush;
    private boolean shield;
    private boolean taunt;
    private boolean poisonous;
    private boolean windFury;
    private boolean lifeSteal;
    private int numberWinFury;
    private boolean update;
    private boolean doubleDamage;
    private String[] MinionTexture;

    public Minion(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, texturePath, lock);
        MinionTexture = new String[2];
        this.HP = HP;
        this.attack = attack;
    }

    public Minion(Minion minion){
        super(minion);
        this.attack = minion.attack;
        this.HP = minion.HP;
        this.charge = minion.charge;
        this.MinionTexture = minion.MinionTexture;
    }

    public static Object factory(LinkedTreeMap map) {
        Class clazz = null;
        try {
            clazz = Class.forName("server.Entity.cards.minion." + EntityUtils.buildName((String) map.get("name")));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Constructor constructor = clazz.getConstructors()[0];
        Double cost = (Double) map.get("cost");
        Double mana = (Double) map.get("mana");
        Double HP = (Double) map.get("HP");
        Double attack = (Double) map.get("attack");
        Object o = null;
        try {
            o = constructor.newInstance(map.get("name"), cost.intValue(), mana.intValue(), Rarity.valueOf((String)map.get("rarity")),
                    HeroClass.valueOf((String)map.get("heroClass")), Type.valueOf((String)map.get("type")), map.get("description"),
                    HP.intValue(), attack.intValue(), EntityUtils.convertArrayListToArray((ArrayList)map.get("texturePath")), map.get("lock"));
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return o;
    }

    public Minion factory2() {
        Class clazz = null;
        try {
            clazz = Class.forName("server.Entity.cards.minion." + EntityUtils.buildName(name));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Constructor constructor = clazz.getConstructors()[0];
        Object o = null;
        try {
            o = constructor.newInstance(name, cost, mana, rarity, heroClass, type, description, HP, attack, texturePath, lock);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return (Minion) o;
    }

    public boolean MinionAttack(Minion minion, GamePlayerLogic GPL) {
        if (!rush) {
            checkLifeSteal(GPL);
            checkPoisonous(minion);
            if(minion.shield){
                minion.shield = false;
                HP = HP - minion.attack;
            }else if (shield){
                shield = false;
                minion.HP = minion.HP - attack;
            } else {
                HP = HP - minion.attack;
                minion.HP = minion.HP - attack;
            }
            return true;
        }
        return false;
    }

    private void checkLifeSteal(GamePlayerLogic GPL){
        if (lifeSteal){
            int heroHP = GPL.getDeck().getHero().getHP();
            int heroMaxHP = GPL.getDeck().getHero().getMAX_HP();
            GPL.getDeck().getHero().setHP(Math.min(heroHP + attack, heroMaxHP));
        }
    }

    private void checkPoisonous(Minion minion){
        if (poisonous){
            if (minion.shield)
                minion.shield = false;
            else minion.HP = 0;
        }
    }

    public boolean MinionAttackHero(GamePlayerLogic GPL){
        if (!charge && !EntityUtils.isThereAnyTaunt(GPL.getContainLandCard())){
            GPL.getDeck().getHero().setHP(GPL.getDeck().getHero().getHP() - attack);
            return true;
        }
        return false;
    }

    public void setCharge(boolean charge) {
        this.charge = charge;
    }

    public Integer getHP() {
        return HP;
    }

    public void setHP(Integer HP) {
        this.HP = HP;
    }

    public Integer getAttack() {
        return attack;
    }

    public String[] getMinionTexture() {
        return MinionTexture;
    }

    public void setMinionTexture(String[] minionTexture) {
        MinionTexture = minionTexture;
    }

    public void setAttack(Integer attack) {
        this.attack = attack;
    }

    public boolean isRush() {
        return rush;
    }

    public void setRush(boolean rush) {
        this.rush = rush;
    }

    public void setShield(boolean shield) {
        this.shield = shield;
    }

    public boolean isShield() {
        return shield;
    }

    public boolean isTaunt() {
        return taunt;
    }

    public void setTaunt(boolean taunt) {
        this.taunt = taunt;
    }

    public boolean isWindFury() {
        return windFury;
    }

    public void setWindFury(boolean windFury) {
        this.windFury = windFury;
    }

    public int getNumberWinFury() {
        return numberWinFury;
    }

    public void setNumberWinFury(int numberWinFury) {
        this.numberWinFury = numberWinFury;
    }

    public boolean isPoisonous() {
        return poisonous;
    }

    public void setPoisonous(boolean poisonous) {
        this.poisonous = poisonous;
    }

    public boolean isLifeSteal() {
        return lifeSteal;
    }

    public void setLifeSteal(boolean lifeSteal) {
        this.lifeSteal = lifeSteal;
    }

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }

    public boolean isDoubleDamage() {
        return doubleDamage;
    }

    public void setDoubleDamage(boolean doubleDamage) {
        this.doubleDamage = doubleDamage;
    }
}
