package server.Entity.cards.minion;

import server.Entity.cards.Rarity;
import server.Entity.cards.Type;
import server.Entity.hero.HeroClass;

public class CurioCollector extends Minion {
    public CurioCollector(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, HP, attack, texturePath, lock);
    }

    public void gain(Minion minion){
        minion.setHP(minion.getHP() + 1);
        minion.setAttack(minion.getAttack() + 1);
        minion.setUpdate(true);
    }
}
