package server.Entity.cards;

public enum Type{
    SPELL,
    MINION,
    WEAPON,
    MISSION
}
