package server.Entity;

import server.Entity.cards.Card;
import server.Entity.cards.Type;
import server.Entity.cards.minion.Minion;
import hearthstone.states.playState.logic.GamePlayerLogic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class EntityUtils {

    public static String reward;

    public static int finedNonZeroRandomIndex(GamePlayerLogic GPL) {
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            if (GPL.getContainLandCard()[i] != null) {
                integers.add(i);
            }
        }
        Collections.shuffle(integers);
        if (integers.size() != 0) {
            return integers.get(0);
        } else return -1;
    }

    public static int finedZeroRandomIndex(GamePlayerLogic GPL) {
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            if (GPL.getContainLandCard()[i] == null) {
                integers.add(i);
            }
        }
        Collections.shuffle(integers);
        if (integers.size() != 0) {
            return integers.get(0);
        } else return -1;
    }

    public static String buildName(String s) {
        String built = "";
        String[] names = s.split(" ");
        for (String name : names) {
            built += name;
        }
        return built;
    }

    public static String[] convertArrayListToArray(ArrayList arrayList) {
        ArrayList<String> strings = new ArrayList<>();
        for (int i = 0; i < arrayList.size(); i++) {
            strings.add((String) arrayList.get(i));
        }
        String[] strings1 = new String[arrayList.size()];
        return strings.toArray(strings1);
    }


    public static void addACardFromDeckToHand(GamePlayerLogic GPL) {
        if (GPL.getDeckIndex() < GPL.getDeck().getCards().size()) {
            GPL.getContainDeckCard().add(GPL.getDeck().getCards().get(GPL.getDeckIndex()));
            GPL.setDeckIndex(GPL.getDeckIndex() + 1);
            GPL.setNumberOfCards(GPL.getNumberOfCards() - 1);
            GPL.changeUpdateFlagDeck();
        }
    }

    public static void decreaseHPFromAllMinion(GamePlayerLogic GPL, int i) {
        for (Minion minion : GPL.getContainLandCard()) {
            if (minion != null) {
                if (minion.isShield()){
                    minion.setShield(false);
                }else {
                    if (minion.isDoubleDamage())
                    minion.setHP(minion.getHP() - 2*i);
                    else minion.setHP(minion.getHP() - i);
                }
            }
        }
        GPL.changeSpecialUpdateFlag();
    }

    public static Card getACardFromDeck(GamePlayerLogic GPL) {
        List<Card> cards = GPL.getDeck().getCards();
        Card card;
        if (GPL.getDeckIndex() < cards.size()) {
            card = cards.get(cards.size() - 1);
            cards.remove(card);
            return card;
        }
        return null;
    }

    public static Minion getAMinionFromDeck(GamePlayerLogic GPL) {
        List<Card> cards = GPL.getDeck().getCards();
        Card card;
        for (int i = GPL.getDeckIndex() + 1; i < cards.size(); i++) {
            card = cards.get(i);
            if (card.getType().equals(Type.MINION)) {
                cards.remove(card);
                return (Minion) card;
            }
        }
        return null;
    }

    public static void addAMinionHPAndAttack(GamePlayerLogic GPL) {
        int x = EntityUtils.finedNonZeroRandomIndex(GPL);
        if (x != -1) {
            GPL.getContainLandCard()[x].setAttack(GPL.getContainLandCard()[x].getAttack() + 1);
            GPL.getContainLandCard()[x].setHP(GPL.getContainLandCard()[x].getHP() + 1);
            GPL.setUpdateFlagACardInLand(x);
        }
    }

    public static boolean isThereAnyTaunt(Minion[] minions){
        for (Minion minion : minions) {
            if (minion.isTaunt()){
                return true;
            }
        }
        return false;
    }

    public static void destroyAllMinion(GamePlayerLogic GPL){
        for (Minion minion : GPL.getContainLandCard()) {
            if (minion != null){
                minion.setHP(0);
            }
        }
        GPL.changeSpecialUpdateFlag();
    }

    public static void makeAllMinionDoubleDamage(GamePlayerLogic GPL){
        for (Minion minion : GPL.getContainLandCard()) {
            if (minion != null){
                minion.setDoubleDamage(true);
            }
        }
    }

    public static void ignoreAllEnemyTaunt(GamePlayerLogic GPL){
        for (Minion minion : GPL.getContainLandCard()) {
            if (minion != null){
                minion.setTaunt(false);
            }
        }
    }

    public static Card detectRandomMinion() {
        Random random = new Random();
        return MainPlayer.getInstance().getEntireCards().get(random.nextInt(17));
    }

    public static String getReward() {
        return reward;
    }

    public static void setReward(String reward) {
        EntityUtils.reward = reward;
    }
}
