package server.Entity;

import server.Entity.cards.Card;
import server.Entity.cards.Type;
import server.Entity.cards.minion.Minion;
import server.Entity.cards.spell.Spell;
import server.Entity.cards.weapon.Weapon;
import server.Entity.hero.*;
import com.google.gson.internal.LinkedTreeMap;
import hearthstone.states.statusState.logic.StatusLogic;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class Deck {
    private List<Card> cards;
    private Hero hero;
    private String texturePath;
    private String name;
    private int numberGame;
    private int numberWin;

    public Deck(List<Card> cards, Hero hero, String texturePath, String name, int numberGame, int numberWin) {
        this.cards = cards;
        this.hero = hero;
        this.texturePath = texturePath;
        this.name = name;
        this.numberGame = numberGame;
        this.numberWin = numberWin;
    }

    public Deck(List<Card> cards, Hero hero) {
        this.cards = cards;
        this.hero = hero;
    }

    public Deck() {
        cards = new ArrayList<>();
        hero = new Mage();
    }

    public Deck(Deck deck) {
        cards = new ArrayList<>();
        for (Card card : deck.getCards()) {
            if (card.getType().equals(Type.MINION)){
                cards.add(((Minion)card).factory2());
            }else if (card.getType().equals(Type.WEAPON)){
                cards.add(((Weapon)card).factory2());
            }else cards.add(card);
        }
        if (deck.getHero().getHeroClass().toString().equals(HeroClass.Mage.toString()))
            this.hero = new Mage(deck.getHero());
        else if (deck.getHero().getHeroClass().toString().equals(HeroClass.Warlock.toString()))
            this.hero = new Warlock(deck.getHero());
        else if (deck.getHero().getHeroClass().toString().equals(HeroClass.Hunter.toString()))
            this.hero = new Hunter(deck.getHero());
        else if (deck.getHero().getHeroClass().toString().equals(HeroClass.Rogue.toString()))
            this.hero = new Rogue(deck.getHero());
        else this.hero = new Paladin(deck.getHero());
        this.texturePath = deck.texturePath;
        this.name = deck.name;
        this.numberGame = deck.numberGame;
        this.numberWin = deck.numberWin;
    }

    public static Object factory(LinkedTreeMap map){
        Class clazz = null;
        try {
            clazz = Class.forName("server.Entity.Deck");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Constructor constructor = clazz.getConstructors()[3];
        List<Card> CARDS = new ArrayList<>();
        ArrayList cards = (ArrayList)(map.get("cards"));
        for (Object card : cards) {
            LinkedTreeMap linkedTreeMap = (LinkedTreeMap)card;
            if (linkedTreeMap.get("type").equals("MINION")){
                CARDS.add((Minion)Minion.factory(linkedTreeMap));
            }else if (linkedTreeMap.get("type").equals("SPELL")){
                CARDS.add((Spell)Spell.factory(linkedTreeMap));
            }else if(linkedTreeMap.get("type").equals("WEAPON")){
                CARDS.add((Weapon)Weapon.factory(linkedTreeMap));
            }
        }
        Hero hero = null;
        for (Hero hero1 : MainPlayer.getInstance().getHeroes()) {
            if ((((LinkedTreeMap)map.get("hero")).get("heroClass")).equals(hero1.getHeroClass().toString())){
                hero = hero1;
                break;
            }
        }
        Double numberGame = (Double) map.get("numberGame");
        Double numberWin = (Double) map.get("numberWin");
        Object o = null;
        try {
            o = constructor.newInstance(CARDS, hero, map.get("texturePath"),map.get("name"), numberGame.intValue(), numberWin.intValue());
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return o;
    }

    public void addCard(Card card) {
        cards.add(card);
    }

    public List<Card> getCards() {
        return cards;
    }

    public Hero getHero() {
        return hero;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    public boolean isCardsNull() {
        return cards == null;
    }

    public String getTexturePath() {
        return texturePath;
    }

    public String getName() {
        return name;
    }

    public void setTexturePath(String texturePath) {
        this.texturePath = texturePath;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberGame() {
        return numberGame;
    }

    public int getNumberWin() {
        return numberWin;
    }

    public boolean isNullHeroClass() {
        return hero.getHeroClass() == null;
    }

    @Override
    public String toString() {
        float a;
        if (numberGame == 0) a = 0;
        else a = (float) numberWin / (float) numberGame;
        String s = "";
        s += "name: " + name + " / ";
        s += "all win divided by all game: " + a + " / ";
        s += "all win: " + numberWin + " / ";
        s += "all games: " + numberGame + " / ";
        s += "mean of cards mana: " + StatusLogic.meanOfMamaDeck(this) + " / ";
        s += "hero: " + hero.getHeroClass() + " / ";
        s += "the most played card: " + StatusLogic.mostPlayedCard(this);
        return s;
    }
}
