package server.data;

import server.Entity.MainPlayer;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

public class Log {

    public static String getTime() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        return simpleDateFormat.format(calendar.getTime());
    }

    public static void createLog(int userId, String username, String password) throws IOException {
        String name = username + "-" + userId +".log";
        FileWriter fileWriter = new FileWriter("logs\\" + name, true);
        fileWriter.write("USER:" + username + "\r\n");
        fileWriter.write("CREATED_AT:" + Log.getTime() + "(timestamp)" + "\r\n");
        fileWriter.write("PASSWORD:" + password + "\r\n");
        fileWriter.write("\r\n");
        fileWriter.flush();
        fileWriter.close();
    }

    public static void body(String event, String description) {
        try{
            String name = MainPlayer.getInstance().getUsername() + "-" + MainPlayer.getInstance().getUserId() +".log";
            FileWriter fileWriter = new FileWriter("logs\\" + name, true);
            fileWriter.write(event + " " + Log.getTime() + " " + description + "\r\n");
            fileWriter.flush();
            fileWriter.close();
        }catch (IOException e){
            e.getStackTrace();
        }
    }

    public static int userId() throws IOException {
        Scanner fileReader = new Scanner(new FileReader("logs/userId.log"));
        String s = fileReader.nextLine();
        fileReader.close();
        int n = Integer.parseInt(s);
        int x = n;
        n++;
        FileWriter fileWriter = new FileWriter("logs/userId1.log", true);
        fileWriter.write(n + "");
        fileWriter.flush();
        fileWriter.close();
        File file = new File("logs/userId.log");
        File file1 = new File("logs/userId1.log");
        file.delete();
        file1.renameTo(file);
        return x;
    }
}
