package server.data;

import com.google.gson.Gson;
import server.Entity.MainPlayer;

import java.io.*;
import java.util.Map;
import java.util.Scanner;

public class MyFile {

    public static void fileWriter(String fileName, Object object) throws IOException {
        String data = new Gson().toJson(object);
        FileWriter fileWriter = new FileWriter(fileName, true);
        fileWriter.write(data + "\r\n");
        fileWriter.flush();
        fileWriter.close();
    }

    public static void deletePlayer(String fileName, String password) throws IOException {
        FileWriter fileWriter = new FileWriter("player1.txt");
        Scanner Reader = new Scanner(new FileReader(fileName));
        while (Reader.hasNext()) {
            String data = Reader.nextLine();
            Gson gson = new Gson();
            Map map = gson.fromJson(data, Map.class);
            if (password.equals(map.get("password"))) continue;
            else {
                fileWriter.write(data + "\r\n");
            }
            fileWriter.flush();
        }
        Reader.close();
        fileWriter.close();
        File file1 = new File("player1.txt");
        File file = new File(fileName);
        file.delete();
        file1.renameTo(file);
    }

    public static boolean isRepetitiveUsername(String username, String fileName) throws FileNotFoundException {
        Scanner fileReader = new Scanner(new FileReader(fileName));
        while (fileReader.hasNext()) {
            Gson gson = new Gson();
            Map map = gson.fromJson(fileReader.nextLine(), Map.class);
            if (map != null) {
                if (map.get("username").equals(username)) {
                    fileReader.close();
                    return true;
                }
            }
        }
        fileReader.close();
        return false;
    }

    public static void saveChanges() {
        try {
            MyFile.deletePlayer("player.txt", MainPlayer.getInstance().getPassword());
            MyFile.fileWriter("player.txt", MainPlayer.getInstance());
        }catch (IOException e){
            e.getStackTrace();
        }
    }

}
