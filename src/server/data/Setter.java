package server.data;

import server.Entity.cards.*;
import server.Entity.cards.minion.*;
import server.Entity.cards.minion.BazaarMugger;
import server.Entity.cards.minion.RotnestDrake;
import server.Entity.cards.spell.*;
import server.Entity.cards.weapon.*;
import server.Entity.cards.minion.TombWarden;
import server.Entity.hero.*;
import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Setter {
    public static List<Hero> setHero() throws FileNotFoundException {
        List<Hero> heroes = new ArrayList<>();
        Scanner fileReader = new Scanner(new FileReader("Heroes.txt"));
        heroes.add(new Gson().fromJson(fileReader.nextLine(), Mage.class));
        heroes.add(new Gson().fromJson(fileReader.nextLine(), Warlock.class));
        heroes.add(new Gson().fromJson(fileReader.nextLine(), Rogue.class));
        heroes.add(new Gson().fromJson(fileReader.nextLine(), Paladin.class));
        heroes.add(new Gson().fromJson(fileReader.nextLine(), Hunter.class));
        fileReader.close();
        return heroes;
    }

    public static ArrayList<Card> setCard() throws FileNotFoundException {
        ArrayList<Card> cards = new ArrayList<>();
        Scanner fileReader = new Scanner(new FileReader("cards.txt"));
        cards.add(new Gson().fromJson(fileReader.nextLine(), TombWarden.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), BazaarMugger.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), RotnestDrake.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), ArchmageArugal.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), Shotbot.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), PitCommander.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), MoargArtificer.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), Doomsayer.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), Pyromaniac.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), Skyvateer.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), Dreadscale.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), SwampKingDred.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), CurioCollector.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), SecurityRover.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), Sathrovarr.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), GadgetzanAuctioneer.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), KaynSunfury.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), AstralRift.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), Polymorph.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), Falmestrike.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), Fireball.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), Slam.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), SenseDemons.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), ArcaneExplosion.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), FriendlySmith.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), SwarmOfLocusts.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), LearnDraconic.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), BookOfSpecters.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), GnomishArmyKnife.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), PharaohsBlessing.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), Sprint.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), StrengthInNumbers.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), WagglePick.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), WickedKnife.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), HeavyAxe.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), BloodFury.class));
        cards.add(new Gson().fromJson(fileReader.nextLine(), SerratedTooth.class));
        fileReader.close();
        return cards;
    }

}
